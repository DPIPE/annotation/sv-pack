import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="svpack",
    version="v1.4.0",
    author="Sjur Urdson Gjerald",
    author_email="s.u.gjerald@medisin.uio.no",
    description="Package of tools for structural variant postprocessing",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/ousamg/apps/sv-pack",
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    extras_require={
        "test": ["pytest"],
    },
    package_dir={"": "src"},
    packages=setuptools.find_packages(where="src"),
    entry_points={
        "console_scripts": [
            "sv_standardizer=svpack.sv_standardizer:main",
            "sv_postprocessing=svpack.sv_postprocessing:main",
            "sv_wgs_filtering=svpack.sv_wgs_filtering:main",
        ],
    },
    python_requires=">=3.6, <4",
)
