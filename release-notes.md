# Release notes

## Version v1.4.0

  - Add standardization of the new `cnv_sv`-file from Dragen
  - Remove the cnvLength filter from the `cnv`-file from Dragen
  - Refactor 'merge'-standardization:
    -  `merge` will now post-process the output of svdb merging.
    -  `header` will just sort the VCF header as the header order is destroyed by many tools.
    -  `ella` will introduce an ELLA compatible variant format

## Version v1.3.0

- Introduce better functionality for making custom filters
- Changes to utils
  - Allow the "search" relation to filters for doing regex search. Primarily meant for crude searching in the CSQ from VEP. Splitting on `,` and and require regex to match all parts.
- Changes to `sv_postprocessing` allow checking for existence of INFO-fields and setting filter exceptions.

## Version v1.2.0

- Introduce more flexible filtering definitions
- Changes to standardization
  - Ensure minimal starting position is base 1
  - Add `CN` from Canvas to the `FORMAT` field when Canvas is merged with Manta

## Version v1.1.0

- Rescue ACMG_class 4,5 and add ACMG_class column to TSV output

## Version v1.0.0

### Changes

Breaking change

- Removes the `--print`-argument from the Python scripts

New features

- Directly installs runnable scripts
  - `sv_standardizer`
  - `sv_postprocessing`
  - `sv_wgs_filtering`
- Moves parsing and formatting of VCFs into `src/sv_utils.py`
- Remove non-variants (`DRAGEN:REF`)
- Add INFO fields `canvasSM`, `canvasPE`, `canvasBC` to back up FORMAT fields `SM`, `PE`, `BC`. Useful for merging.

## Version v0.9.0

Version `v0.9.0` of this repository was never released, but is a collection of the Python scripts hardcoded in the following repositories

- `svtools@v0.1.2`
- `anno-targets@74a1b0f8a4f7045b8e8a628d059c2c0927e558d3`
