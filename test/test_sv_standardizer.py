import pytest
import io
import itertools as it
from svpack import sv_utils as utils

from svpack.sv_standardizer import CALLERS
from svpack.sv_standardizer import standardize_header
from svpack.sv_standardizer import add_headers_standardize
from svpack.sv_standardizer import standardize
from svpack.sv_standardizer import do_sv_standardizing


###################
# Header testdata #
###################
HEADER_PRE = """##fileformat=VCFv4.1
##INFO=<ID=END,Number=1,Type=String,Description="End position of the variant described in this record">
##INFO=<ID=SVLEN,Number=1,Type=Integer,Description="Difference in length between REF and ALT alleles">
##INFO=<ID=HOMLEN,Number=1,Type=Integer,Description="Length of base pair identical homology at event breakpoints">
##INFO=<ID=JUNCTION_QUAL,Number=1,Type=Integer,Description="If the SV junction is part of an EVENT (ie. a multi-adjacency variant), this field provides the QUAL value for the adjacency in question only">
##FILTER=<ID=PASS,Description="All filters passed">
##FORMAT=<ID=DR,Number=1,Type=Integer,Description="# high-quality reference pairs">
##FORMAT=<ID=RR,Number=2,Type=Integer,Description="# high-quality reference junction reads">
##FORMAT=<ID=PE,Number=1,Type=Integer,Description="Number of paired-ends that support the event">
#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	HTEST"""

HEADER_PRE_MERGED = """##fileformat=VCFv4.1
##INFO=<ID=END,Number=1,Type=String,Description="End position of the variant described in this record">
##INFO=<ID=SVLEN,Number=1,Type=Integer,Description="Difference in length between REF and ALT alleles">
##INFO=<ID=HOMLEN,Number=1,Type=Integer,Description="Length of base pair identical homology at event breakpoints">
##INFO=<ID=Diag-wgs2-HG002_std.sv_INFO,Number=.,Type=String,Description="pipe separated list of all details in the INFO column of file Diag-wgs2-HG002_std.sv">
##INFO=<ID=Diag-wgs2-HG002_std.sv_SAMPLE,Number=.,Type=String,Description="pipe separated list of all details in the SAMPLEs column of file Diag-wgs2-HG002_std.sv">
##INFO=<ID=Diag-wgs2-HG002_std.sv_CHROM,Number=.,Type=String,Description="pipe separated list of all details in the CHROM column of file Diag-wgs2-HG002_std.sv">
##INFO=<ID=Diag-wgs2-HG002_std.sv_POS,Number=.,Type=String,Description="pipe separated list of all details in the POS column of file Diag-wgs2-HG002_std.sv">
##INFO=<ID=Diag-wgs2-HG002_std.sv_QUAL,Number=.,Type=String,Description="pipe separated list of all details in the QUAL column of file Diag-wgs2-HG002_std.sv">
##INFO=<ID=Diag-Validation2-HG002_std.sv_FILTERS,Number=.,Type=String,Description="pipe separated list of all details in the FILTER column of file Diag-Validation2-HG002_std.sv">
##INFO=<ID=JUNCTION_QUAL,Number=1,Type=Integer,Description="If the SV junction is part of an EVENT (ie. a multi-adjacency variant), this field provides the QUAL value for the adjacency in question only">
##FILTER=<ID=PASS,Description="All filters passed">
##FORMAT=<ID=DR,Number=1,Type=Integer,Description="# high-quality reference pairs">
##FORMAT=<ID=RR,Number=2,Type=Integer,Description="# high-quality reference junction reads">
##FORMAT=<ID=PE,Number=1,Type=Integer,Description="Number of paired-ends that support the event">
#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	HTEST"""

HEADER_POST = """##fileformat=VCFv4.1
##INFO=<ID=END,Number=1,Type=Integer,Description="End position of the variant described in this record">
##INFO=<ID=SVLEN,Number=.,Type=Integer,Description="Difference in length between REF and ALT alleles">
##INFO=<ID=HOMLEN,Number=.,Type=Integer,Description="Length of base pair identical homology at event breakpoints">
##INFO=<ID=DELLYID,Number=1,Type=String,Description="DELLY ID value for variant">
##INFO=<ID=MANTAID,Number=1,Type=String,Description="MANTA ID value for variant">
##INFO=<ID=TIDDITID,Number=1,Type=String,Description="TIDDIT ID value for variant">
##INFO=<ID=CNVNATORID,Number=1,Type=String,Description="CNVNATOR ID value for variant">
##INFO=<ID=CANVASID,Number=1,Type=String,Description="CANVAS ID value for variant">
##INFO=<ID=CNV_SVID,Number=1,Type=String,Description="CNV_SV ID value for variant">
##INFO=<ID=DELLYFILTER,Number=.,Type=String,Description="DELLY FILTER value for variant">
##INFO=<ID=MANTAFILTER,Number=.,Type=String,Description="MANTA FILTER value for variant">
##INFO=<ID=TIDDITFILTER,Number=.,Type=String,Description="TIDDIT FILTER value for variant">
##INFO=<ID=CNVNATORFILTER,Number=.,Type=String,Description="CNVNATOR FILTER value for variant">
##INFO=<ID=CANVASFILTER,Number=.,Type=String,Description="CANVAS FILTER value for variant">
##INFO=<ID=CNV_SVFILTER,Number=.,Type=String,Description="CNV_SV FILTER value for variant">
##INFO=<ID=canvasSM,Number=.,Type=Float,Description="All observed linear copy ratios of the segment mean for record">
##INFO=<ID=canvasPE,Number=.,Type=Integer,Description="All observed numbers of improperly paired end reads at start and stop breakpoints for record">
##INFO=<ID=canvasBC,Number=.,Type=Integer,Description="All observed number of bins in the region for record">
##INFO=<ID=canvasCN,Number=1,Type=Integer,Description="Estimated copy number">
##INFO=<ID=JUNCTION_QUAL,Number=1,Type=Integer,Description="If the SV junction is part of an EVENT (ie. a multi-adjacency variant), this field provides the QUAL value for the adjacency in question only">
##FILTER=<ID=PASS,Description="All filters passed">
##FILTER=<ID=HomRef,Description="homozygous reference call (filter applied at sample level)">
##FORMAT=<ID=DR,Number=.,Type=Integer,Description="# high-quality reference pairs">
##FORMAT=<ID=RR,Number=.,Type=Integer,Description="# high-quality reference junction reads">
##FORMAT=<ID=PE,Number=2,Type=Integer,Description="Number of improperly paired end reads at start and stop breakpoints">
#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	HG002"""

HEADER_REMOVED = """##INFO=<ID=Diag-wgs2-HG002_std.sv_INFO,Number=.,Type=String,Description="pipe separated list of all details in the INFO column of file Diag-wgs2-HG002_std.sv">
##INFO=<ID=Diag-wgs2-HG002_std.sv_SAMPLE,Number=.,Type=String,Description="pipe separated list of all details in the SAMPLEs column of file Diag-wgs2-HG002_std.sv">
##INFO=<ID=Diag-wgs2-HG002_std.sv_CHROM,Number=.,Type=String,Description="pipe separated list of all details in the CHROM column of file Diag-wgs2-HG002_std.sv">
##INFO=<ID=Diag-wgs2-HG002_std.sv_POS,Number=.,Type=String,Description="pipe separated list of all details in the POS column of file Diag-wgs2-HG002_std.sv">
##INFO=<ID=Diag-wgs2-HG002_std.sv_QUAL,Number=.,Type=String,Description="pipe separated list of all details in the QUAL column of file Diag-wgs2-HG002_std.sv">
##INFO=<ID=Diag-Validation2-HG002_std.sv_FILTERS,Number=.,Type=String,Description="pipe separated list of all details in the FILTER column of file Diag-Validation2-HG002_std.sv">"""

############################
# Testdata variant records #
############################
BODY_PRE = {
    "cnvnator": [
        "1	1	CNVnator_del_1	N	<DEL>	.	PASS	END=9000;SVTYPE=DEL;SVLEN=-9000;IMPRECISE;natorRD=0;natorP1=1.77081e+11;natorP2=0;natorP3=2.27675e-11;natorP4=0;natorQ0=-1	GT:CN	1/1:0",
        "1	1	CNVnator_del_2	N	<DEL>	.	PASS	END=9000;SVTYPE=DEL;SVLEN=-9000;IMPRECISE;natorRD=0.03;natorP1=1.77081e+11;natorP2=0;natorP3=2.27675e-11;natorP4=0;natorQ0=0.9	GT:CN	1/1:0",
        "1	9376	CNVnator_dup_2	N	<DUP>	.	PASS	END=21000;SVTYPE=DUP;SVLEN=11625;IMPRECISE;natorRD=3.29332;natorP1=0.0512962;natorP2=2.87097e+09;natorP3=2.97219e-05;natorP4=2.87085e+09;natorQ0=0.68213	GT:CN	./1:3",
    ],
    "manta": [
        "1	1649043	MantaDEL:8686:0:0:0:0:1	CGCTTTCAGCTAGAGTTTGCTCTCTCTGGTTTTCGGTCTGTGACACACGCAT	C	501	PASS	END=1649094;SVTYPE=DEL;SVLEN=-51;CIGAR=1M51D;CIPOS=0,50;HOMLEN=50;HOMSEQ=GCTTTCAGCTAGAGTTTGCTCTCTCTGGTTTTCGGTCTGTGACACACGCA	GT:FT:GQ:PL:PR:SR	1/1:PASS:35:554,38,0:0,0:0,15",
        "1	1649043	MantaDUP:8686:0:0:0:0:1	C	CGCTTTCAGCTAGAGTTTGCTCTCTCTGGTTTTCGGTCTGTGACACACGCAT	501	PASS	END=1649094;SVTYPE=DUP;SVLEN=51;CIGAR=1M51D;CIPOS=0,50;HOMLEN=50;HOMSEQ=GCTTTCAGCTAGAGTTTGCTCTCTCTGGTTTTCGGTCTGTGACACACGCA	GT:FT:GQ:PL:PR:SR	1/1:PASS:35:554,38,0:0,15:0,1",
        "1	1649043	MantaBND:9006:0:1:0:0:0:0	C	C]2:134420863]	501	PASS	SVTYPE=BND;MATEID=MantaBND:9006:0:1:0:0:0:1;CIPOS=0,5;HOMLEN=5;HOMSEQ=TTTTT;BND_DEPTH=67;MATE_BND_DEPTH=51	GT:FT:GQ:PL:PR:SR	1/1:PASS:35:554,38,0:0,15:0,15",
    ],
    "delly": [
        "1	756271	DEL00000004	T	<DEL>	48	LowQual	IMPRECISE;SVTYPE=DEL;SVMETHOD=EMBL.DELLYv0.8.3;END=756365;PE=2;MAPQ=24;CT=3to5;CIPOS=-337,337;CIEND=-337,337	GT:GL:GQ:FT:RCL:RC:RCR:CN:DR:DV:RR:RV	0/0:0,-3.32213,-136.598:33:PASS:190:368:176:2:25:2:0:0",
        "1	756271	DUP00000004	T	<DUP>	48	PASS	IMPRECISE;SVTYPE=DUP;SVMETHOD=EMBL.DELLYv0.8.3;END=758365;PE=2;MAPQ=24;CT=3to5;CIPOS=-337,337;CIEND=-337,337	GT:GL:GQ:FT:RCL:RC:RCR:CN:DR:DV:RR:RV	0/1:0,-3.32213,-136.598:33:PASS:190:368:176:2:25:2:0:0",
        "1	756271	INS00000004	T	<INS>	48	PASS	PRECISE;SVTYPE=INS;SVMETHOD=EMBL.DELLYv0.8.3;END=2002491;PE=0;MAPQ=0;CT=NtoN;CIPOS=-21,21;CIEND=-21,21;SRMAPQ=60;INSLEN=626;HOMLEN=20;SR=4;SRQ=0.986486	GT:GL:GQ:FT:RCL:RC:RCR:CN:DR:DV:RR:RV	0/1:0,-3.32213,-136.598:33:PASS:190:368:176:2:25:6:0:0",
        "2	24796758	BND00001046	G	[1:84517943[G	976	PASS	PRECISE;SVTYPE=BND;SVMETHOD=EMBL.DELLYv0.8.3;END=24796759;CHR2=1;POS2=84517943;PE=13;MAPQ=49;CT=5to5;CIPOS=-3,3;CIEND=-3,3;SRMAPQ=60;INSLEN=0;HOMLEN=3;SR=7;SRQ=1;CE=1.67826	GT:GL:GQ:FT:RCL:RC:RCR:CN:DR:DV:RR:RV	0/1:-89.2604,0,-23.3861:10000:PASS:20357:43498:23141:2:0:16:9:30",
    ],
    "tiddit": [
        "1	16890824	SV_49_1	N	<TDUP>	3	PASS	SVTYPE=TDUP;CIPOS=0,60;CIEND=-288,0;END=16893985;SVLEN=3162;COVM=273.932159424;COVA=157.327484131;COVB=250.527923584;LFA=2;LFB=2;LTE=1;OR=0,0,18,0;ORSR=0,1;QUALA=7;QUALB=46	GT:CN:DV:RV:DR:RR	1/1:15:4:1:167,139:106,88",
        "1	16890824	SV_49_2	N	<DEL>	3	PASS	SVTYPE=DEL;CIPOS=0,60;CIEND=-288,0;END=16893985;SVLEN=3162;COVM=273.932159424;COVA=157.327484131;COVB=250.527923584;LFA=2;LFB=2;LTE=1;OR=0,0,18,0;ORSR=0,1;QUALA=77;QUALB=4	GT:CN:DV:RV:DR:RR	1/1:2:4:1:167,139:106,88",
    ],
    "canvas": [
        "1	14437061	DRAGEN:LOSS:1:14437062-14438764	N	<DEL>	16	cnvLength	SVTYPE=CNV;END=14438764;REFLEN=1703	GT:SM:CN:BC:PE	0/1:0.367212:1:6:36,49",
        "1	0	DRAGEN:GAIN:1:1-11704	N	<DUP>	16	PASS	SVTYPE=CNV;END=11704;REFLEN=11703	GT:SM:CN:BC:PE	0/1:0.367212:1:6:36,49",
        "1	14437061	DRAGEN:REF:1:14437062-14448764	N	.	16	PASS	END=14448764;REFLEN=11703	GT:SM:CN:BC:PE	0/1:0.367212:1:6:36,49",
    ],
    "merged": [
        (
            "1	72766323	DRAGEN:DEL:29115:0:1:0:0:0	T	<DEL>	999	PASS	"
            "END=72811839;SVTYPE=DEL;SVLEN=-45516;CIPOS=0,1;CIEND=0,1;HOMLEN=1;HOMSEQ=G;MANTAID=DRAGEN:DEL:29115:0:1:0:0:0;MANTAFILTER=PASS;REFLEN=45172;canvasSM=9.98834e-10;canvasPE=40,31;canvasBC=41;canvasCN=0;CANVASID=DRAGEN:LOSS:1:72766095-72811266;CANVASFILTER=PASS;"
            "Diag-wgs2-HG002_std.sv_CHROM=DRAGEN_DEL_29115_0_1_0_0_0|1;Diag-wgs2-HG002_std.cnv_CHROM=DRAGEN_LOSS_1_72766095-72811266|1;Diag-wgs2-HG002_std.sv_POS=DRAGEN_DEL_29115_0_1_0_0_0|72766323;Diag-wgs2-HG002_std.cnv_POS=DRAGEN_LOSS_1_72766095-72811266|72766094;Diag-wgs2-HG002_std.sv_QUAL=DRAGEN_DEL_29115_0_1_0_0_0|999;"
            "Diag-wgs2-HG002_std.cnv_QUAL=DRAGEN_LOSS_1_72766095-72811266|94;Diag-wgs2-HG002_std.sv_FILTERS=DRAGEN_DEL_29115_0_1_0_0_0|PASS;Diag-wgs2-HG002_std.cnv_FILTERS=DRAGEN_LOSS_1_72766095-72811266|PASS;Diag-wgs2-HG002_std.sv_SAMPLE=DRAGEN_DEL_29115_0_1_0_0_0|Diag-wgs380-NA12878K4|GT:1/1|FT:PASS|GQ:133|PL:999:136:0|PR:0:17|SR:0:31;"
            "Diag-wgs2-HG002_std.cnv_SAMPLE=DRAGEN_LOSS_1_72766095-72811266|Diag-wgs380-NA12878K4|GT:1/1|SM:9.98834e-10|CN:0|BC:41|GC:0.354954|CT:0.505977|AC:0.491056|PE:40:31;Diag-wgs2-HG002_std.sv_INFO=DRAGEN_DEL_29115_0_1_0_0_0|END:72811839|SVTYPE:DEL|SVLEN:-45516|CIPOS:0:1|CIEND:0:1|HOMLEN:1|HOMSEQ:G|MANTAFILTER:PASS;"
            "Diag-wgs2-HG002_std.cnv_INFO=DRAGEN_LOSS_1_72766095-72811266|SVLEN:-45172|SVTYPE:DEL|END:72811266|REFLEN:45172|canvasSM:9.98834e-10|canvasPE:40:31|canvasBC:41|canvasCN:0|CANVASFILTER:PASS;SUPP_VEC=11	GT:FT:GQ:PL:PR:SR	"
            "1/1:PASS:133:999,136,0:0,17:0,31"
        ),
        (
            "6	35754560	DRAGEN:DUP:TANDEM:60638:0:1:0:0:0	A	<DUP:TANDEM>	999	PASS	"
            "END=35766785;SVTYPE=DUP;SVLEN=12225;CIPOS=0,3;CIEND=0,3;HOMLEN=3;HOMSEQ=AAC;MANTAID=DRAGEN:DUP:TANDEM:60638:0:1:0:0:0;MANTAFILTER=cnvLength;REFLEN=12539;canvasSM=1.55936;canvasPE=34,37;canvasBC=11;canvasCN=3;CANVASID=DRAGEN:GAIN:6:35754230-35766768;CANVASFILTER=cnvLength;"
            "Diag-Validation2-HG002_std.sv_CHROM=DRAGEN_DUP_TANDEM_60638_0_1_0_0_0|6;Diag-wgs2-HG002_std.cnv_CHROM=DRAGEN_GAIN_6_35754230-35766768|6;Diag-wgs2-HG002_std.sv_POS=DRAGEN_DUP_TANDEM_60638_0_1_0_0_0|35754560;Diag-wgs2-HG002_std.cnv_POS=DRAGEN_GAIN_6_35754230-35766768|35754229;"
            "Diag-Validation2-HG002_std.sv_QUAL=DRAGEN_DUP_TANDEM_60638_0_1_0_0_0|999;Diag-wgs2-HG002_std.cnv_QUAL=DRAGEN_GAIN_6_35754230-35766768|39;Diag-wgs2-HG002_std.sv_FILTERS=DRAGEN_DUP_TANDEM_60638_0_1_0_0_0|PASS;Diag-wgs2-HG002_std.cnv_FILTERS=DRAGEN_GAIN_6_35754230-35766768|PASS;"
            "Diag-Validation2-HG002_std.sv_SAMPLE=DRAGEN_DUP_TANDEM_60638_0_1_0_0_0|Diag-wgs380-NA12878K4|GT:0/1|FT:PASS|GQ:999|PL:999:0:999|PR:58:16|SR:65:23;Diag-wgs2-HG002_std.cnv_SAMPLE=DRAGEN_GAIN_6_35754230-35766768|Diag-wgs380-NA12878K4|GT:./1|SM:1.55936|CN:3|BC:11|GC:0.506739|CT:0.472286|AC:0.49693|PE:34:37;"
            "Diag-Validation2-HG002_std.sv_INFO=DRAGEN_DUP_TANDEM_60638_0_1_0_0_0|END:35766785|SVTYPE:DUP|SVLEN:12225|CIPOS:0:3|CIEND:0:3|HOMLEN:3|HOMSEQ:AAC|MANTAFILTER:cnvLength;Diag-wgs2-HG002_std.cnv_INFO=DRAGEN_GAIN_6_35754230-35766768|SVLEN:12539|SVTYPE:DUP|END:35766768|REFLEN:12539|canvasSM:1.55936|canvasPE:34:37|canvasBC:11|canvasCN:3|CANVASFILTER:cnvLength;SUPP_VEC=11	GT:FT:GQ:PL:PR:SR	"
            "0/1:PASS:999:999,0,999:58,16:65,23"
        ),
    ],
    "ella": [
        "1	14437061	DRAGEN:GAIN:1:14437062-14448764	N	<DUP>	16	PASS	SVTYPE=DUP;END=14448764;REFLEN=11703;canvasSM=0.367212;canvasPE=36,49;canvasBC=6;canvasCN=1;SVLEN=11703;CANVASID=DRAGEN:GAIN:1:14437062-14448764	GT:SM:CN:BC:PE	0/1:0.367212:1:6:36,49",
        "1	9376	CNVnator_dup_2	N	<DUP>	.	PASS	END=21000;SVTYPE=DUP;SVLEN=11625;IMPRECISE;natorRD=3.29332;natorP1=0.0512962;natorP2=2.87097e+09;natorP3=2.97219e-05;natorP4=2.87085e+09;natorQ0=0.68213;CNVNATORID=CNVnator_dup_2	GT:CN	./1:7",
        "6	35754560	DRAGEN:DUP:TANDEM:60638:0:1:0:0:0	A	<DUP:TANDEM>	999	PASS	END=35766785;SVTYPE=DUP;SVLEN=12225;CIPOS=0,3;CIEND=0,3;HOMLEN=3;HOMSEQ=AAC;MANTAID=DRAGEN:DUP:TANDEM:60638:0:1:0:0:0;MANTAFILTER=PASS;REFLEN=12539;canvasSM=1.55936;canvasPE=34,37;canvasBC=11;canvasCN=3;CANVASID=DRAGEN:GAIN:6:35754230-35766768;CANVASFILTER=PASS	GT:FT:GQ:PL:PR:SR	0/1:PASS:999:999,0,999:58,16:65,23",
    ],
    "header": [
        "1	14437061	DRAGEN:GAIN:1:14437062-14448764	N	<DUP>	16	PASS	SVTYPE=DUP;END=14448764;REFLEN=11703;canvasSM=0.367212;canvasPE=36,49;canvasBC=6;canvasCN=1;SVLEN=11703;CANVASID=DRAGEN:GAIN:1:14437062-14448764	GT:SM:CN:BC:PE	0/1:0.367212:1:6:36,49",
    ],
    "cnv_sv": [
        "1	16151940	DRAGEN:LOSS:1:16151941-16155439	N	<DEL>	150	PASS	END=16155439;SVTYPE=DEL;SVLEN=-3499;SVINSLEN=15;SVINSSEQ=GGGCCTTCCAAAATA;END_RIGHT_BND_OF=DRAGEN:LOSS:1:16149606-16155439;END_LEFT_BND_OF=DRAGEN:REF:1:16155439-16855217;SVCLAIM=J	GT:FT:GQ:PL:PR:SR	0/1:PASS:322:373,0,331:20,4:29,10",
        "1	58743909	DRAGEN:LOSS:1:58743910-58744822	N	<DEL>	150	PASS	SVLEN=-913;SVTYPE=CNV;END=58744822;REFLEN=913;OrigCnvPos=58743528;OrigCnvEnd=58744672;SVCLAIM=DJ;MatchSv=DRAGEN:DEL:28339:0:1:0:0:0	GT:SM:CN:BC:GC:CT:AC:PE	1/1:0.15462:0:1:0.446678:0.548077:0.491259:36,83",
    ],
}

FILTER_POST = {
    "cnvnator": ["PASS", "PASS", "PASS"],
    "manta": ["PASS", "PASS", "PASS"],
    "delly": ["LowQual;HomRef", "PASS", "PASS", "PASS"],
    "tiddit": ["PASS", "PASS"],
    "canvas": ["PASS", "PASS", None],
    "merged": ["PASS", "PASS"],
    "ella": ["PASS", "PASS", "PASS"],
    "header": ["PASS"],
    "cnv_sv": ["PASS", "PASS"],
}

SVLEN_POST = {
    "cnvnator": ["-9000", "-9000", "11625"],
    "manta": ["-51", "51", "-1"],
    "delly": ["-94", "2094", "626", "-1"],
    "tiddit": ["3162", "-3162"],
    "canvas": ["-1703", "11703", None],
    "merged": ["-45516", "12225"],
    "ella": ["11703", "11625", "12225"],
    "header": ["11703"],
    "cnv_sv": ["-3499", "-913"],
}

GT_POST = {
    "cnvnator": ["1/1", "1/1", "./1"],
    "manta": ["1/1", "1/1", "1/1"],
    "delly": ["0/0", "0/1", "0/1", "0/1"],
    "tiddit": ["1/1", "1/1"],
    "canvas": ["0/1", "0/1", None],
    "merged": ["1/1", "0/1"],
    "ella": ["./.", "./.", "0/1"],
    "header": ["0/1"],
    "cnv_sv": ["0/1", "1/1"],
}

BODY_POST = {
    "cnvnator": [
        "1	1	CNVnator_del_1	N	<DEL>	.	PASS	END=9000;SVTYPE=DEL;SVLEN=-9000;IMPRECISE;natorRD=0;natorP1=1.77081e+11;natorP2=0;natorP3=2.27675e-11;natorP4=0;natorQ0=-1;CNVNATORID=CNVnator_del_1;CNVNATORFILTER=PASS	GT:CN	1/1:0",
        "1	1	CNVnator_del_2	N	<DEL>	.	PASS	END=9000;SVTYPE=DEL;SVLEN=-9000;IMPRECISE;natorRD=0.03;natorP1=1.77081e+11;natorP2=0;natorP3=2.27675e-11;natorP4=0;natorQ0=0.9;CNVNATORID=CNVnator_del_2;CNVNATORFILTER=PASS	GT:CN	1/1:0",
        "1	9376	CNVnator_dup_2	N	<DUP>	.	PASS	END=21000;SVTYPE=DUP;SVLEN=11625;IMPRECISE;natorRD=3.29332;natorP1=0.0512962;natorP2=2.87097e+09;natorP3=2.97219e-05;natorP4=2.87085e+09;natorQ0=0.68213;CNVNATORID=CNVnator_dup_2;CNVNATORFILTER=PASS	GT:CN	./1:7",
    ],
    "manta": [
        "1	1649043	MantaDEL:8686:0:0:0:0:1	CGCTTTCAGCTAGAGTTTGCTCTCTCTGGTTTTCGGTCTGTGACACACGCAT	C	501	PASS	END=1649094;SVTYPE=DEL;SVLEN=-51;CIGAR=1M51D;CIPOS=0,50;HOMLEN=50;HOMSEQ=GCTTTCAGCTAGAGTTTGCTCTCTCTGGTTTTCGGTCTGTGACACACGCA;MANTAID=MantaDEL:8686:0:0:0:0:1;MANTAFILTER=PASS	GT:FT:GQ:PL:PR:SR	1/1:PASS:35:554,38,0:0,0:0,15",
        "1	1649043	MantaDUP:8686:0:0:0:0:1	C	CGCTTTCAGCTAGAGTTTGCTCTCTCTGGTTTTCGGTCTGTGACACACGCAT	501	PASS	END=1649094;SVTYPE=DUP;SVLEN=51;CIGAR=1M51D;CIPOS=0,50;HOMLEN=50;HOMSEQ=GCTTTCAGCTAGAGTTTGCTCTCTCTGGTTTTCGGTCTGTGACACACGCA;MANTAID=MantaDUP:8686:0:0:0:0:1;MANTAFILTER=PASS	GT:FT:GQ:PL:PR:SR	1/1:PASS:35:554,38,0:0,15:0,1",
        "1	1649043	MantaBND:9006:0:1:0:0:0:0	C	C]2:134420863]	501	PASS	SVTYPE=BND;MATEID=MantaBND:9006:0:1:0:0:0:1;CIPOS=0,5;HOMLEN=5;HOMSEQ=TTTTT;BND_DEPTH=67;MATE_BND_DEPTH=51;SVLEN=-1;MANTAID=MantaBND:9006:0:1:0:0:0:0;MANTAFILTER=PASS	GT:FT:GQ:PL:PR:SR	1/1:PASS:35:554,38,0:0,15:0,15",
    ],
    "delly": [
        "1	756271	DEL00000004	T	<DEL>	48	LowQual;HomRef	IMPRECISE;SVTYPE=DEL;SVMETHOD=EMBL.DELLYv0.8.3;END=756365;PE=2;MAPQ=24;CT=3to5;CIPOS=-337,337;CIEND=-337,337;SVLEN=-94;DELLYID=DEL00000004;DELLYFILTER=LowQual,HomRef	GT:GL:GQ:FT:RCL:RC:RCR:CN:DR:DV:RR:RV	0/0:0,-3.32213,-136.598:33:PASS:190:368:176:2:25:2:0:0",
        "1	756271	DUP00000004	T	<DUP:TANDEM>	48	PASS	IMPRECISE;SVTYPE=DUP;SVMETHOD=EMBL.DELLYv0.8.3;END=758365;PE=2;MAPQ=24;CT=3to5;CIPOS=-337,337;CIEND=-337,337;SVLEN=2094;DELLYID=DUP00000004;DELLYFILTER=PASS	GT:GL:GQ:FT:RCL:RC:RCR:CN:DR:DV:RR:RV	0/1:0,-3.32213,-136.598:33:PASS:190:368:176:2:25:2:0:0",
        "1	756271	INS00000004	T	<INS>	48	PASS	PRECISE;SVTYPE=INS;SVMETHOD=EMBL.DELLYv0.8.3;END=2002491;PE=0;MAPQ=0;CT=NtoN;CIPOS=-21,21;CIEND=-21,21;SRMAPQ=60;INSLEN=626;HOMLEN=20;SR=4;SRQ=0.986486;SVLEN=626;DELLYID=INS00000004;DELLYFILTER=PASS	GT:GL:GQ:FT:RCL:RC:RCR:CN:DR:DV:RR:RV	0/1:0,-3.32213,-136.598:33:PASS:190:368:176:2:25:6:0:0",
        "2	24796758	BND00001046	G	[1:84517943[G	976	PASS	PRECISE;SVTYPE=BND;SVMETHOD=EMBL.DELLYv0.8.3;CHR2=1;POS2=84517943;PE=13;MAPQ=49;CT=5to5;CIPOS=-3,3;CIEND=-3,3;SRMAPQ=60;INSLEN=0;HOMLEN=3;SR=7;SRQ=1;CE=1.67826;SVLEN=-1;DELLYID=BND00001046;DELLYFILTER=PASS	GT:GL:GQ:FT:RCL:RC:RCR:CN:DR:DV:RR:RV	0/1:-89.2604,0,-23.3861:10000:PASS:20357:43498:23141:2:0:16:9:30",
    ],
    "tiddit": [
        "1	16890824	SV_49_1	N	<DUP:TANDEM>	3	PASS	SVTYPE=DUP;CIPOS=0,60;CIEND=-288,0;END=16893985;SVLEN=3162;COVM=273.932159424;COVA=157.327484131;COVB=250.527923584;LFA=2;LFB=2;LTE=1;OR=0,0,18,0;ORSR=0,1;QUALA=7;QUALB=46;TIDDITID=SV_49_1;TIDDITFILTER=PASS	GT:CN:DV:RV:DR:RR	1/1:15:4:1:167,139:106,88",
        "1	16890824	SV_49_2	N	<DEL>	3	PASS	SVTYPE=DEL;CIPOS=0,60;CIEND=-288,0;END=16893985;SVLEN=-3162;COVM=273.932159424;COVA=157.327484131;COVB=250.527923584;LFA=2;LFB=2;LTE=1;OR=0,0,18,0;ORSR=0,1;QUALA=77;QUALB=4;TIDDITID=SV_49_2;TIDDITFILTER=PASS	GT:CN:DV:RV:DR:RR	1/1:2:4:1:167,139:106,88",
    ],
    "canvas": [
        "1	14437061	DRAGEN:LOSS:1:14437062-14438764	N	<DEL>	16	PASS	SVTYPE=DEL;END=14438764;REFLEN=1703;canvasSM=0.367212;canvasPE=36,49;canvasBC=6;canvasCN=1;SVLEN=-1703;CANVASID=DRAGEN:LOSS:1:14437062-14438764;CANVASFILTER=cnvLength	GT:SM:CN:BC:PE	0/1:0.367212:1:6:36,49",
        "1	1	DRAGEN:GAIN:1:1-11704	N	<DUP>	16	PASS	SVTYPE=DUP;END=11704;REFLEN=11703;canvasSM=0.367212;canvasPE=36,49;canvasBC=6;canvasCN=1;SVLEN=11703;CANVASID=DRAGEN:GAIN:1:1-11704;CANVASFILTER=PASS	GT:SM:CN:BC:PE	0/1:0.367212:1:6:36,49",
        "",
    ],
    "merged": [
        (
            "1	72766323	DRAGEN:DEL:29115:0:1:0:0:0	T	<DEL>	999	PASS	"
            "END=72811839;SVTYPE=DEL;SVLEN=-45516;CIPOS=0,1;CIEND=0,1;HOMLEN=1;HOMSEQ=G;MANTAID=DRAGEN:DEL:29115:0:1:0:0:0;MANTAFILTER=PASS;REFLEN=45172;canvasSM=9.98834e-10;canvasPE=40,31;canvasBC=41;canvasCN=0;CANVASID=DRAGEN:LOSS:1:72766095-72811266;CANVASFILTER=PASS;SUPP_VEC=11	"
            "GT:FT:GQ:PL:PR:SR:CN	1/1:PASS:133:999,136,0:0,17:0,31:0"
        ),
        (
            "6	35754560	DRAGEN:DUP:TANDEM:60638:0:1:0:0:0	A	<DUP:TANDEM>	999	PASS	"
            "END=35766785;SVTYPE=DUP;SVLEN=12225;CIPOS=0,3;CIEND=0,3;HOMLEN=3;HOMSEQ=AAC;MANTAID=DRAGEN:DUP:TANDEM:60638:0:1:0:0:0;MANTAFILTER=cnvLength;REFLEN=12539;canvasSM=1.55936;canvasPE=34,37;canvasBC=11;canvasCN=3;CANVASID=DRAGEN:GAIN:6:35754230-35766768;CANVASFILTER=cnvLength;SUPP_VEC=11	"
            "GT:FT:GQ:PL:PR:SR:CN	0/1:PASS:999:999,0,999:58,16:65,23:3"
        ),
    ],
    "ella": [
        "1	14437061	DRAGEN:GAIN:1:14437062-14448764	N	<DUP>	16	PASS	SVTYPE=DUP;END=14448764;REFLEN=11703;canvasSM=0.367212;canvasPE=36,49;canvasBC=6;canvasCN=1;SVLEN=11703;CANVASID=DRAGEN:GAIN:1:14437062-14448764	GT:SM:CN:BC:PE	./.:0.367212:1:6:36,49",
        "1	9376	CNVnator_dup_2	N	<DUP>	.	PASS	END=21000;SVTYPE=DUP;SVLEN=11625;IMPRECISE;natorRD=3.29332;natorP1=0.0512962;natorP2=2.87097e+09;natorP3=2.97219e-05;natorP4=2.87085e+09;natorQ0=0.68213;CNVNATORID=CNVnator_dup_2	GT:CN	./.:7",
        "6	35754560	DRAGEN:DUP:TANDEM:60638:0:1:0:0:0	A	<DUP:TANDEM>	999	PASS	END=35766785;SVTYPE=DUP;SVLEN=12225;CIPOS=0,3;CIEND=0,3;HOMLEN=3;HOMSEQ=AAC;MANTAID=DRAGEN:DUP:TANDEM:60638:0:1:0:0:0;MANTAFILTER=PASS;REFLEN=12539;canvasSM=1.55936;canvasPE=34,37;canvasBC=11;canvasCN=3;CANVASID=DRAGEN:GAIN:6:35754230-35766768;CANVASFILTER=PASS	GT:FT:GQ:PL:PR:SR	0/1:PASS:999:999,0,999:58,16:65,23",
    ],
    "header": [
        "1	14437061	DRAGEN:GAIN:1:14437062-14448764	N	<DUP>	16	PASS	SVTYPE=DUP;END=14448764;REFLEN=11703;canvasSM=0.367212;canvasPE=36,49;canvasBC=6;canvasCN=1;SVLEN=11703;CANVASID=DRAGEN:GAIN:1:14437062-14448764	GT:SM:CN:BC:PE	0/1:0.367212:1:6:36,49",
    ],
    "cnv_sv": [
        "1	16151940	DRAGEN:LOSS:1:16151941-16155439	N	<DEL>	150	PASS	END=16155439;SVTYPE=DEL;SVLEN=-3499;SVINSLEN=15;SVINSSEQ=GGGCCTTCCAAAATA;END_RIGHT_BND_OF=DRAGEN:LOSS:1:16149606-16155439;END_LEFT_BND_OF=DRAGEN:REF:1:16155439-16855217;SVCLAIM=J;CNV_SVID=DRAGEN:LOSS:1:16151941-16155439;CNV_SVFILTER=PASS	GT:FT:GQ:PL:PR:SR	0/1:PASS:322:373,0,331:20,4:29,10",
        "1	58743909	DRAGEN:LOSS:1:58743910-58744822	N	<DEL>	150	PASS	SVLEN=-913;SVTYPE=DEL;END=58744822;REFLEN=913;OrigCnvPos=58743528;OrigCnvEnd=58744672;SVCLAIM=DJ;MatchSv=DRAGEN:DEL:28339:0:1:0:0:0;CNV_SVID=DRAGEN:LOSS:1:58743910-58744822;CNV_SVFILTER=PASS	GT:SM:CN:BC:GC:CT:AC:PE	1/1:0.15462:0:1:0.446678:0.548077:0.491259:36,83",
    ],
}

TEST_CALLERS = [
    "manta",
    "delly",
    "tiddit",
    "cnvnator",
    "canvas",
    "merged",
    "header",
    "ella",
    "cnv_sv",
]


# Test header standardization not merged
@pytest.mark.parametrize(
    "header_pre", standardize_header(HEADER_PRE.split("\n"), ["HG002"], "any_caller_not_merged")
)
def test_standardize_header(header_pre):
    assert utils.format_header(*header_pre) in HEADER_POST


# Test header standardization merged
@pytest.mark.parametrize(
    "header_pre", standardize_header(HEADER_PRE_MERGED.split("\n"), ["HG002"], "merged")
)
def test_standardize_header_merged(header_pre):
    assert utils.format_header(*header_pre) not in HEADER_REMOVED
    assert utils.format_header(*header_pre) in HEADER_POST


# Test addition of header definitions
@pytest.mark.parametrize("caller", TEST_CALLERS)
def test_add_header(caller):
    header_pre = HEADER_PRE.split("\n")
    header_post = HEADER_POST.split("\n")

    header = set(
        utils.complement_header(
            standardize_header(header_pre, ["HG002"], caller),
            add_headers_standardize(caller, known_callers=CALLERS),
        )
    )

    assert header, header
    assert header.issubset(set(header_post)), header


# Test main function doing header standardization
@pytest.mark.parametrize("caller", TEST_CALLERS)
def test_main_header(caller):
    header_post = HEADER_POST.split("\n")
    header = set(
        do_sv_standardizing(io.StringIO(str(HEADER_PRE)), ["HG002"], caller, known_callers=CALLERS)
    )
    assert header, header
    assert header.issubset(
        set(header_post)
    ), f"{caller}: {header} -> {header-set(header_post)} -> {set(header_post)-header}"


# Common function to make pairs of pre-data and post-data
def make_body_line_pairs(post_data):
    # Tuple format: (caller, (body_pre, body_post))
    data_tuples = map(lambda c: it.product([c], zip(BODY_PRE[c], post_data[c])), TEST_CALLERS)
    return it.chain.from_iterable(data_tuples)


# Test standardization
@pytest.mark.parametrize("body_line_pair", make_body_line_pairs(BODY_POST))
def test_standardize_record(body_line_pair):
    caller, (pre_line, post_line) = body_line_pair
    assert standardize(pre_line, caller, known_callers=CALLERS) == post_line, utils.parse_record(
        pre_line
    )


# Test that do_sv_standardizing does not affect standardization
@pytest.mark.parametrize("body_line_pair", make_body_line_pairs(BODY_POST))
def test_main_record(body_line_pair):
    caller, (pre_line, post_line) = body_line_pair
    assert (
        "\n".join(
            do_sv_standardizing(
                io.StringIO(str(pre_line)), ["HG002"], caller, known_callers=CALLERS
            )
        )
        == post_line
    ), utils.parse_record(pre_line)


# Test FILTER standardization
@pytest.mark.parametrize("body_line_filter", make_body_line_pairs(FILTER_POST))
def test_standardization_filter(body_line_filter):
    caller, (pre_line, post_filter) = body_line_filter
    out_line = standardize(pre_line, caller, known_callers=CALLERS)
    if out_line:
        record, _ = utils.parse_record(out_line)
        assert record["filter"] == post_filter, record
    else:
        assert out_line == ""


# Test SVLEN standardization
@pytest.mark.parametrize("body_line_svlen", make_body_line_pairs(SVLEN_POST))
def test_get_standardized_svlen(body_line_svlen):
    caller, (pre_line, post_svlen) = body_line_svlen
    out_line = standardize(pre_line, caller, known_callers=CALLERS)
    if out_line:
        record, _ = utils.parse_record(out_line)
        assert record["info"]["SVLEN"] == post_svlen, record
    else:
        assert out_line == ""


# Test GT standardization
@pytest.mark.parametrize("body_line_gt", make_body_line_pairs(GT_POST))
def test_get_standardized_gt(body_line_gt):
    caller, (pre_line, post_gt) = body_line_gt

    out_line = standardize(pre_line, caller, known_callers=CALLERS)
    if out_line:
        sample_col = out_line.split("\t")[9]
        assert sample_col.startswith(post_gt), post_gt
    else:
        assert out_line == ""
