import pytest
import io
import itertools as it

from svpack.sv_utils import parse_record
from svpack.sv_postprocessing import add_quality_filter
from svpack.sv_postprocessing import do_sv_postprocessing

###################################
# Test values for quality filters #
###################################
QUALITY_FILTERS = {
    "MinSize": {
        "cnvnator": {"MinSize1": {"info:SVLEN": {"le": 0}}},  # ca 6 times CNVnator bin size
        "delly": {"MinSize1": {"info:SVLEN": {"le": 0}}},  # 300, but that will remove INS
    },
    "MaxQ0": {
        "cnvnator": {"MaxQ0_1": {"info:natorQ0": {"gt": 0.2}}},
        "exceptions": {
            "undefinedQ0": {"info:natorQ0": {"in": ["-1"]}},
            "homozygoteDEL": {"info:natorRD": {"le": 0.03}},
        },
    },
    "MaxP": {"cnvnator": {"MaxP1_2": {"info:natorP1": {"gt": 0.05}, "info:natorP2": {"gt": 0.05}}}},
    "LowEvidence": {
        "tiddit": {"LowEvidence1": {"format:DV": {"le": 5}, "format:RV": {"le": 5}}},
        "delly": {
            "LowEvidenceINS": {"format:RV": {"le": 5}, "info:SVTYPE": {"in": ["INS"]}},
            "LowEvidenceDELDUP": {"format:DV": {"le": 5}, "info:SVTYPE": {"in": ["DEL", "DUP"]}},
        },
    },
    "EvidenceConflict": {
        "delly": {
            "EvidenceConflictDEL": {"info:SVTYPE": {"in": ["DEL"]}, "format:CN": {"gt": 1}},
            "EvidenceConflictDUP": {"info:SVTYPE": {"in": ["DUP"]}, "format:CN": {"lt": 3}},
        },
        "tiddit": {
            "EvidenceConflictDEL": {"info:SVTYPE": {"in": ["DEL"]}, "format:CN": {"gt": 1}},
            "EvidenceConflictDUP": {"info:SVTYPE": {"in": ["DUP"]}, "format:CN": {"lt": 3}},
        },
        "exceptions": {"undefined_CN": {"format:CN": {"in": ["."]}}},
    },
    "HomRef": {"delly": {"HomRef1": {"format:GT": {"in": ["0/0"]}}}},
    "MinSizeRepetitive": {
        "common": {
            "MinSizeAnnotatedWithRepeatType": {
                "info:SVLEN": {"le": 300},
                "info:Repeat_type_right": {},
                "info:Repeat_type_left": {},
            }
        },
        "exceptions": {
            "non_repetitive_region": {
                "info:Repeat_type_right": {"in": ["-1"]},
                "info:Repeat_type_left": {"in": ["-1"]},
            }
        },
    },
    "IntronVariant": {
        "common": {
            "MinSizeIntronVariant": {
                "info:SVLEN": {"le": 300},
                "info:CSQ": {"search": "\\|intron_variant\\|"},
            }
        }
    },
    "cnvLength5k": {
        "common": {
            "set_cnvLength_filter_for_canvas": {
                "info:SVLEN": {"le": 5000},
                "id": {"search": "^DRAGEN:(LOSS|GAIN)"},
            }
        }
    },
}

FILTER_DESCRIPTIONS = {
    "MinSize": "The variant is smaller than the user set limit",
    "MaxQ0": "Too high fraction of MAPQ0-reads",
    "MaxP": "Too hight E-value P1 or P2",
    "LowEvidence": "Too few discordant reads support event",
    "EvidenceConflict": "Discordant reads evidence disagree with read depth for unbalanced event",
    "HomRef": "homozygous reference call (filter applied at sample level)",
}

HEADER_PRE = {"total": ["""#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	HTEST"""]}

HEADER_POST = {
    "total": [
        """##FILTER=<ID=MinSize,Description="The variant is smaller than the user set limit">
##FILTER=<ID=MaxQ0,Description="Too high fraction of MAPQ0-reads">
##FILTER=<ID=MaxP,Description="Too hight E-value P1 or P2">
##FILTER=<ID=LowEvidence,Description="Too few discordant reads support event">
##FILTER=<ID=EvidenceConflict,Description="Discordant reads evidence disagree with read depth for unbalanced event">
##FILTER=<ID=HomRef,Description="homozygous reference call (filter applied at sample level)">
##FILTER=<ID=MinSizeRepetitive,Description="Custom filter MinSizeRepetitive">
##FILTER=<ID=IntronVariant,Description="Custom filter IntronVariant">
##FILTER=<ID=cnvLength5k,Description="Custom filter cnvLength5k">
#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	HTEST"""
    ]
}

# Caller-specific VCF record line
BODY_PRE = {
    "cnvnator": [
        "1	1	CNVnator_del_1	N	<DEL>	.	PASS	END=9000;SVTYPE=DEL;SVLEN=-9000;IMPRECISE;natorRD=0;natorP1=1.77081e+11;natorP2=0;natorP3=2.27675e-11;natorP4=0;natorQ0=-1;CNVNATORID=CNVnator_del_1	GT:CN	1/1:0",
        "1	1	CNVnator_del_2	N	<DEL>	.	PASS	END=9000;SVTYPE=DEL;SVLEN=-9000;IMPRECISE;natorRD=0.03;natorP1=1.77081e+11;natorP2=0;natorP3=2.27675e-11;natorP4=0;natorQ0=0.9;CNVNATORID=CNVnator_del_2	GT:CN	1/1:0",
        "1	9376	CNVnator_dup_2	N	<DUP>	.	PASS	END=21000;SVTYPE=DUP;SVLEN=11625;IMPRECISE;natorRD=3.29332;natorP1=0.0512962;natorP2=2.87097e+09;natorP3=2.97219e-05;natorP4=2.87085e+09;natorQ0=0.68213;CNVNATORID=CNVnator_dup_2	GT:CN	./1:7",
    ],
    "manta": [
        "1	1649043	MantaDEL:8686:0:0:0:0:1	CGCTTTCAGCTAGAGTTTGCTCTCTCTGGTTTTCGGTCTGTGACACACGCAT	C	501	PASS	END=1649094;SVTYPE=DEL;SVLEN=-51;CIGAR=1M51D;CIPOS=0,50;HOMLEN=50;HOMSEQ=GCTTTCAGCTAGAGTTTGCTCTCTCTGGTTTTCGGTCTGTGACACACGCA;MANTAID=MantaDEL:8686:0:0:0:0:1;Repeat_type_right=(CCC);Repeat_type_left=Alu;CSQ=-|intron_variant||,-|intron_variant|	GT:FT:GQ:PL:PR:SR	1/1:PASS:35:554,38,0:0,0:0,15",
        "1	1649043	MantaDUP:8686:0:0:0:0:1	C	CGCTTTCAGCTAGAGTTTGCTCTCTCTGGTTTTCGGTCTGTGACACACGCAT	501	PASS	END=1649094;SVTYPE=DUP;SVLEN=51;CIGAR=1M51D;CIPOS=0,50;HOMLEN=50;HOMSEQ=GCTTTCAGCTAGAGTTTGCTCTCTCTGGTTTTCGGTCTGTGACACACGCA;MANTAID=MantaDUP:8686:0:0:0:0:1;CNVNATORID=CNVnator_dup_9;Repeat_type_right=-1;Repeat_type_left=Alu;CSQ=-|intron_variant||,-|intron_variant&other|	GT:FT:GQ:PL:PR:SR	1/1:PASS:35:554,38,0:0,15:0,1",
        "1	1649043	MantaBND:9006:0:1:0:0:0:0	C	C]2:134420863]	501	PASS	SVTYPE=BND;MATEID=MantaBND:9006:0:1:0:0:0:1;CIPOS=0,5;HOMLEN=5;HOMSEQ=TTTTT;BND_DEPTH=67;MATE_BND_DEPTH=51;SVLEN=-1;MANTAID=MantaBND:9006:0:1:0:0:0:0;Repeat_type_right=-1;Repeat_type_left=-1	GT:FT:GQ:PL:PR:SR	1/1:PASS:35:554,38,0:0,15:0,15",
    ],
    "delly": [
        "1	756271	DEL00000004	T	<DEL>	48	LowQual;HomRef	IMPRECISE;SVTYPE=DEL;SVMETHOD=EMBL.DELLYv0.8.3;END=756365;PE=2;MAPQ=24;CT=3to5;CIPOS=-337,337;CIEND=-337,337;SVLEN=-94;DELLYID=DEL00000004	GT:GL:GQ:FT:RCL:RC:RCR:CN:DR:DV:RR:RV	0/0:0,-3.32213,-136.598:33:PASS:190:368:176:2:25:2:0:0",
        "1	756271	DUP00000004	T	<DUP:TANDEM>	48	PASS	IMPRECISE;SVTYPE=DUP;SVMETHOD=EMBL.DELLYv0.8.3;END=758365;PE=2;MAPQ=24;CT=3to5;CIPOS=-337,337;CIEND=-337,337;SVLEN=2094;DELLYID=DUP00000004	GT:GL:GQ:FT:RCL:RC:RCR:CN:DR:DV:RR:RV	0/1:0,-3.32213,-136.598:33:PASS:190:368:176:2:25:2:0:0",
        "1	756271	INS00000004	T	<INS>	48	PASS	PRECISE;SVTYPE=INS;SVMETHOD=EMBL.DELLYv0.8.3;END=2002491;PE=0;MAPQ=0;CT=NtoN;CIPOS=-21,21;CIEND=-21,21;SRMAPQ=60;INSLEN=626;HOMLEN=20;SR=4;SRQ=0.986486;SVLEN=626;DELLYID=INS00000004	GT:GL:GQ:FT:RCL:RC:RCR:CN:DR:DV:RR:RV	0/1:0,-3.32213,-136.598:33:PASS:190:368:176:2:25:6:0:0",
        "2	24796758	BND00001046	G	[1:84517943[G	976	PASS	PRECISE;SVTYPE=BND;SVMETHOD=EMBL.DELLYv0.8.3;CHR2=1;POS2=84517943;PE=13;MAPQ=49;CT=5to5;CIPOS=-3,3;CIEND=-3,3;SRMAPQ=60;INSLEN=0;HOMLEN=3;SR=7;SRQ=1;CE=1.67826;SVLEN=-1;DELLYID=BND00001046	GT:GL:GQ:FT:RCL:RC:RCR:CN:DR:DV:RR:RV	0/1:-89.2604,0,-23.3861:10000:PASS:20357:43498:23141:2:0:16:9:30",
    ],
    "tiddit": [
        "1	16890824	SV_49_1	N	<DUP:TANDEM>	3	PASS	SVTYPE=DUP;CIPOS=0,60;CIEND=-288,0;END=16893985;SVLEN=3162;COVM=273.932159424;COVA=157.327484131;COVB=250.527923584;LFA=2;LFB=2;LTE=1;OR=0,0,18,0;ORSR=0,1;QUALA=7;QUALB=46;TIDDITID=SV_49_1;DELLYID=DUP00000008	GT:CN:DV:RV:DR:RR	1/1:15:4:1:167,139:106,88",
        "1	16890824	SV_49_2	N	<DEL>	3	PASS	SVTYPE=DEL;CIPOS=0,60;CIEND=-288,0;END=16893985;SVLEN=-3162;COVM=273.932159424;COVA=157.327484131;COVB=250.527923584;LFA=2;LFB=2;LTE=1;OR=0,0,18,0;ORSR=0,1;QUALA=77;QUALB=4;TIDDITID=SV_49_2	GT:CN:DV:RV:DR:RR	1/1:2:4:1:167,139:106,88",
    ],
    "canvas": [
        "1	14437061	DRAGEN:LOSS:1:14437062-14438764	N	<DEL>	16	cnvLength	SVTYPE=DEL;END=14438764;REFLEN=1703;CANVASID=DRAGEN:LOSS:1:14437062-14438764;SVLEN=-1703	GT:SM:CN:BC:PE	0/1:0.367212:1:6:36,49",
        "1	14437061	DRAGEN:GAIN:1:14437062-14448764	N	<DUP>	16	PASS	SVTYPE=DUP;END=14448764;REFLEN=11703;SVLEN=11703;CANVASID=DRAGEN:GAIN:1:14437062-14448764	GT:SM:CN:BC:PE	0/1:0.367212:1:6:36,49",
    ],
    "merged": [
        "1	14437061	DRAGEN:LOSS:1:14437062-14438764	N	<DEL>	16	PASS	SVTYPE=DEL;END=14438764;REFLEN=1703;CANVASID=DRAGEN:LOSS:1:14437062-14438764;SVLEN=-1703;CANVASFILTER=cnvLength	GT:SM:CN:BC:PE	0/1:0.367212:1:6:36,49",
        "1	14437061	DRAGEN:GAIN:1:14437062-14448764	N	<DUP>	16	PASS	SVTYPE=DUP;END=14448764;REFLEN=11703;SVLEN=11703;CANVASID=DRAGEN:GAIN:1:14437062-14448764;CANVASFILTER=PASS	GT:SM:CN:BC:PE	0/1:0.367212:1:6:36,49",
        "1	14437061	DRAGEN:DEL:1:14437062-14438764	N	<DEL>	16	PASS	SVTYPE=DEL;END=14438764;REFLEN=1703;CANVASID=DRAGEN:LOSS:1:14437062-14438764;MANTAID=DRAGEN:DEL:1:14437062-14438764;SVLEN=-1703;CANVASFILTER=cnvLength;MANTAFILTER=PASS	GT:SM:CN:BC:PE	0/1:0.367212:1:6:36,49",
    ],
}


# Expected value in VCF FILTER column after frequency annotation
FILTER_POST = {
    "cnvnator": ["PASS", "PASS", "MaxQ0;MaxP"],
    "manta": ["MinSizeRepetitive;IntronVariant", "MinSizeRepetitive", "PASS"],
    "delly": [
        "LowQual;HomRef;LowEvidence;EvidenceConflict",
        "LowEvidence;EvidenceConflict",
        "LowEvidence",
        "PASS",
    ],
    "tiddit": ["LowEvidence", "LowEvidence;EvidenceConflict"],
    "canvas": ["cnvLength;cnvLength5k", "PASS"],
    "merged": ["cnvLength5k", "PASS", "PASS"],
}


CALLER_PRIORITY = ["manta", "canvas", "tiddit", "delly", "cnvnator"]


# Make pairs of input and output
def make_in_out_pairs(pre_data, post_data):
    return zip(
        it.chain.from_iterable(pre_data.values()), it.chain.from_iterable(post_data.values())
    )


# Check that frequency tags are added to FILTER
@pytest.mark.parametrize("pre_line, post_filter", make_in_out_pairs(BODY_PRE, FILTER_POST))
def test_add_quality_filter(pre_line, post_filter):
    (record, samples) = parse_record(pre_line)
    assert (
        add_quality_filter(
            record, samples, filters=QUALITY_FILTERS, caller_priority=CALLER_PRIORITY
        )
        == post_filter
    ), pre_line


# Check that filters are correctly added
@pytest.mark.parametrize("pre_line, post_filter", make_in_out_pairs(BODY_PRE, FILTER_POST))
def test_do_sv_postprocessing_add_quality_filter(pre_line, post_filter):
    kwargs = dict(
        filters=QUALITY_FILTERS,
        caller_priority=CALLER_PRIORITY,
        filter_descriptions=FILTER_DESCRIPTIONS,
    )
    formatted_line = "\n".join(do_sv_postprocessing(io.StringIO(str(pre_line)), **kwargs))

    assert pre_line.split("\t")[7:] == formatted_line.split("\t")[7:], pre_line
    assert pre_line.split("\t")[:6] == formatted_line.split("\t")[:6], pre_line
    assert formatted_line.split("\t")[6] == post_filter, pre_line


# Check that resulting header is correct
@pytest.mark.parametrize("pre_header, post_header", make_in_out_pairs(HEADER_PRE, HEADER_POST))
def test_add_complement_header(pre_header, post_header):
    kwargs = dict(
        filters=QUALITY_FILTERS,
        caller_priority=CALLER_PRIORITY,
        filter_descriptions=FILTER_DESCRIPTIONS,
    )
    formatted_line = "\n".join(do_sv_postprocessing(io.StringIO(str(pre_header)), **kwargs))
    assert formatted_line == post_header
