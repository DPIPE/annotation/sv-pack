import pytest
from collections import OrderedDict

from svpack.sv_utils import parse_header
from svpack.sv_utils import format_header
from svpack.sv_utils import complement_header
from svpack.sv_utils import parse_record
from svpack.sv_utils import format_record

from svpack.sv_utils import filter_body
from svpack.sv_utils import check_filter_relation, check_filter_relations
from svpack.sv_utils import check_sample_relations, check_relations
from svpack.sv_utils import parse_relation, check_relation


###################
# Header testdata #
###################
HEADER = """##fileformat=VCFv4.1
##INFO=<ID=SVLEN,Number=1,Type=Integer,Description="Difference in length between REF and ALT alleles">
##FILTER=<ID=PASS,Description="All filters passed">
##FORMAT=<ID=DR,Number=1,Type=Integer,Description="# high-quality reference pairs">
##contig=<ID=1,length=249250621>
##FORMAT=<ID=PE,Number=1,Type=Integer,Description="Number of paired-ends that support the event">
##ALT=<ID=DEL,Description="Deletion">
#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	HTEST"""

HEADER_INTERNAL = {
  "##fileformat=VCFv4.1": {
    "CUSTOM": "##fileformat=VCFv4.1"
  },
  "##INFO=<ID=SVLEN": {
    "ID": "SVLEN",
    "Number": "1",
    "Type": "Integer",
    "Description": "\"Difference in length between REF and ALT alleles\""
  },
  "##FILTER=<ID=PASS": {
    "ID": "PASS",
    "Description": "\"All filters passed\""
  },
  "##FORMAT=<ID=DR": {
    "ID": "DR",
    "Number": "1",
    "Type": "Integer",
    "Description": "\"# high-quality reference pairs\""
  },
  "##contig=<ID=1": {
    "ID": "1",
    "length": "249250621"
  },
  "##FORMAT=<ID=PE": {
    "ID": "PE",
    "Number": "1",
    "Type": "Integer",
    "Description": "\"Number of paired-ends that support the event\""
  },
  "##ALT=<ID=DEL": {
    "ID": "DEL",
    "Description": "\"Deletion\""
  },
  "#CHROM": {
    "SAMPLES": [
      "HTEST"
    ]
  }
}


HEADER_ADDED_INTERNAL = {
  "##fileformat=VCFv4.1": {
    "CUSTOM": "##fileformat=VCFv4.1"
  },
  "##INFO=<ID=SVLEN": {
    "ID": "SVLEN",
    "Number": "1",
    "Type": "Integer",
    "Description": "\"Difference in length between REF and ALT alleles\""
  },
  "##FILTER=<ID=PASS": {
    "ID": "PASS",
    "Description": "\"All filters passed\""
  },
  "##FORMAT=<ID=SR": {
    "ID": "SR",
    "Number": ".",
    "Type": "Integer",
    "Description": "\"# high-quality split reads\""
  },
  "##contig=<ID=2": {
    "ID": "2",
    "length": "243199373"
  },
  "##FORMAT=<ID=PE": {
    "ID": "PE",
    "Number": "1",
    "Type": "Integer",
    "Description": "\"Number of paired-ends that support the event\""
  },
  "##ALT=<ID=DUP": {
    "ID": "DUP",
    "Description": "\"Duplication\""
  },
  "#CHROM": {
    "SAMPLES": [
      "HTEST"
    ]
  }
}


HEADER_COMBINED = """##fileformat=VCFv4.1
##INFO=<ID=SVLEN,Number=1,Type=Integer,Description="Difference in length between REF and ALT alleles">
##FILTER=<ID=PASS,Description="All filters passed">
##FORMAT=<ID=DR,Number=1,Type=Integer,Description="# high-quality reference pairs">
##FORMAT=<ID=PE,Number=1,Type=Integer,Description="Number of paired-ends that support the event">
##FORMAT=<ID=SR,Number=.,Type=Integer,Description="# high-quality split reads">
##contig=<ID=1,length=249250621>
##contig=<ID=2,length=243199373>
##ALT=<ID=DEL,Description="Deletion">
##ALT=<ID=DUP,Description="Duplication">
#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	HTEST"""


LINE = "1	13273	.	G	<DEL>	295.55	VQSR;PASS	END=20000;SVLEN=-1000;SVTYPE=DEL;CANVASID=DRAGEN123	GT:AD:DP:GQ:PL	0/1:3,0:3:9:0,9,113	0/0:12,0:12:33:0,33,495"

LINE_INTERNAL_RECORD = {'chrom': '1',
                        'pos': '13273',
                        'id': '.',
                        'ref': 'G',
                        'alt': '<DEL>',
                        'qual': '295.55',
                        'filter': 'VQSR;PASS',
                        'info': OrderedDict([('END', '20000'),
                                             ('SVLEN', '-1000'),
                                             ('SVTYPE', 'DEL'),
                                             ('CANVASID', 'DRAGEN123')]),
                        'format': 'GT:AD:DP:GQ:PL'}
LINE_INTERNAL_SAMPLES = [OrderedDict([('GT', '0/1'),
                                      ('AD', '3,0'),
                                      ('DP', '3'),
                                      ('GQ', '9'),
                                      ('PL', '0,9,113')]),
                         OrderedDict([('GT', '0/0'),
                                      ('AD', '12,0'),
                                      ('DP', '12'),
                                      ('GQ', '33'),
                                      ('PL', '0,33,495')])]


FLEXIBLE_FILTER = {
    "small": {
        "info:SVTYPE": {
            "in": ["DEL"]
            },
        "info:SVLEN": {
            "lt": 2000
            }
        },
    "medium": {
        "info:SVTYPE": {
            "in": ["DEL"]
            },
        "info:SVLEN": {
            "gt": 50,
            "lt": 50000
            }
    },
    "canvas": {
        "info:OCC_INDB": {
            "le": 10
            },
        "info:AN_INDB": {
            "gt": 100
            }
    },
    "exception": {
        "info:SVTYPE": {
            "in": ["DEL"]
        },
        "chrom": {
            "in": ["1"]
            },
        "format:GT": {
            "in": ["0/0"]
        }
    },
    "include_filter": {
        "filter": {
            "contains": ["PASS"]
        }
    }
}

RELATION = [["1", "in", ["1"], True],
            ["d|intron|,-|intron|", "search", "\\|intron\\|", True],
            ["d|intron|,-|intron&more|", "search", "\\|intron\\|", False],
            ["PASS;.", "contains", ["PASS", "PA"], True],
            ["PASS;.", "contains", ["PA", ""], False],
            ["10", "le", 10, True],
            ["10", "gt", "9.1", True],
            ["10", "eq", "10.01", False]]


RELATIONS = [[100, FLEXIBLE_FILTER["medium"]["info:SVLEN"], True],
             [10, FLEXIBLE_FILTER["medium"]["info:SVLEN"], False],
             [100000, FLEXIBLE_FILTER["medium"]["info:SVLEN"], False]]


SAMPLE_RELATIONS = [
  [LINE_INTERNAL_SAMPLES, "GT", FLEXIBLE_FILTER["exception"]["format:GT"], True]
  ]


FILTER_RELATION = [
  [LINE_INTERNAL_RECORD, LINE_INTERNAL_SAMPLES, "info:SVLEN", FLEXIBLE_FILTER["medium"]["info:SVLEN"], True],
  [LINE_INTERNAL_RECORD, LINE_INTERNAL_SAMPLES, "chrom", FLEXIBLE_FILTER["exception"]["chrom"], True]
  ]


FILTER_RELATIONS = [
  [LINE_INTERNAL_RECORD, LINE_INTERNAL_SAMPLES, FLEXIBLE_FILTER["medium"], True],
  [LINE_INTERNAL_RECORD, LINE_INTERNAL_SAMPLES, FLEXIBLE_FILTER["exception"], True],
  [LINE_INTERNAL_RECORD, LINE_INTERNAL_SAMPLES, FLEXIBLE_FILTER["canvas"], False]
]


FILTER_BODY = [
  [LINE_INTERNAL_RECORD, LINE_INTERNAL_SAMPLES, FLEXIBLE_FILTER, ["medium"], True],
  [LINE_INTERNAL_RECORD, LINE_INTERNAL_SAMPLES, FLEXIBLE_FILTER, ["medium", "canvas"], True],
  [LINE_INTERNAL_RECORD, LINE_INTERNAL_SAMPLES, FLEXIBLE_FILTER, ["canvas"], False],
  [LINE_INTERNAL_RECORD, LINE_INTERNAL_SAMPLES, FLEXIBLE_FILTER, [], True]
]


@pytest.mark.parametrize("value, relation, filter_value, _out", RELATION)
def test_check_relation(value, relation, filter_value, _out):
    assert check_relation(*parse_relation(value, relation, filter_value)) == _out


@pytest.mark.parametrize("_value, filter_relations, _out", RELATIONS)
def test_check_relations(_value, filter_relations, _out):
    assert check_relations(_value, filter_relations) == _out


@pytest.mark.parametrize("samples, vcf_column, filter_relations, _out", SAMPLE_RELATIONS)
def test_check_sample_relations(samples, vcf_column, filter_relations, _out):
    assert check_sample_relations(samples, vcf_column, filter_relations) == _out


@pytest.mark.parametrize("record, samples, filter_type, filter_relations, _out", FILTER_RELATION)
def test_check_filter_relation(record, samples, filter_type, filter_relations, _out):
    assert check_filter_relation(record, samples, filter_type, filter_relations) == _out


@pytest.mark.parametrize("record, samples, filters, _out", FILTER_RELATIONS)
def test_check_filter_relations(record, samples, filters, _out):
    assert check_filter_relations(record, samples, filters) == _out


@pytest.mark.parametrize("record, samples, filters, group_names, _out", FILTER_BODY)
def test_filter_body(record, samples, filters, group_names, _out):
    assert filter_body(record, samples, filters, group_names=group_names) == _out


# Test header parsing
@pytest.mark.parametrize("header", HEADER.split("\n"))
def test_parse_header(header):
    (_type, value_dict) = parse_header(header)
    assert HEADER_INTERNAL[_type] == value_dict, value_dict


# Test header formatting
@pytest.mark.parametrize("header_item, header", zip(HEADER_INTERNAL.items(), HEADER.split('\n')))
def test_format_header(header_item, header):
    _type, value_dict = header_item
    assert header == format_header(_type, value_dict)


# Test addition of headers
def test_add_header():
    header_internal = complement_header(HEADER_INTERNAL.items(),
                                        HEADER_ADDED_INTERNAL.items())
    header_combined = "\n".join(header_internal)
    assert header_combined == HEADER_COMBINED


# Test parsing variant record
@pytest.mark.parametrize("line, record, samples", [(LINE, LINE_INTERNAL_RECORD, LINE_INTERNAL_SAMPLES)])
def test_parse_record(line, record, samples):
    assert parse_record(LINE) == (record, samples)


# Test formatting variant record
@pytest.mark.parametrize("line, record, samples", [(LINE, LINE_INTERNAL_RECORD, LINE_INTERNAL_SAMPLES)])
def test_format_record(line, record, samples):
    assert format_record(record, samples) == LINE
