import pytest
import io
import itertools as it

from svpack.sv_utils import parse_record
from svpack.sv_wgs_filtering import add_frequency_filter
from svpack.sv_wgs_filtering import frequency_filter
from svpack.sv_wgs_filtering import format_evidence
from svpack.sv_wgs_filtering import format_record_tsv
from svpack.sv_wgs_filtering import select_for_interpretation
from svpack.sv_wgs_filtering import do_sv_wgs_filtering


###############################################################
# Test values for frequency filters and interpretation groups #
###############################################################
INTERPRETATION_GROUPS = {
    "small": {
        "info:SVLEN": {"lt": 2000},
        "info:SVTYPE": {"in": ["DEL", "DUP"]},  # Add SVTYPEs
        "filter": {"contains": ["PASS"]}
    },
    "medium": {
        "info:SVLEN": {"ge": 2000,
                       "lt": 10000},
        "info:SVTYPE": {"in": ["DEL", "DUP"]},
        "filter": {"contains": ["PASS"]}
    },
    "large": {
        "info:SVLEN": {"ge": 10000},
        "info:SVTYPE": {"in": ["DEL", "DUP"]},
        "filter": {"contains": ["PASS"]}
    },
    "default": {}
}

FREQUENCY_FILTERS = {
    "common": {
        "gnomad": {
            "info:FRQ_GNOMAD": {"gt": 0.005},
            "info:OCC_GNOMAD": {"gt": 5}
        },
    },
    "manta": {
        "swegen": {
            "info:OCC_SWEGEN_MANTA": {"gt": 5},
            "info:FRQ_SWEGEN_MANTA": {"gt": 0.005}
        },
        "indb": {
            "info:OCC_INDB_MANTA": {"gt": 5},
            "info:FRQ_INDB_MANTA": {"gt": 0.005}
        }
        },
    "canvas": {
        "swegen": {
            "info:OCC_SWEGEN_CNVNATOR": {"gt": 5},
            "info:FRQ_SWEGEN_CNVNATOR": {"gt": 0.005}
        },
        "indb": {
            "info:OCC_INDB_CANVAS": {"gt": 5},
            "info:FRQ_INDB_CANVAS": {"gt": 0.005}
        }
        },
    "cnvnator": {
        "swegen": {
            "info:OCC_SWEGEN_CNVNATOR": {"gt": 5},
            "info:FRQ_SWEGEN_CNVNATOR": {"gt": 0.005}
        },
        "indb": {
            "info:OCC_INDB_CNVNATOR": {"gt": 5},
            "info:FRQ_INDB_CNVNATOR": {"gt": 0.005}
        }
        },
    "delly": {
        "swegen": {
            "info:OCC_SWEGEN_DELLY": {"gt": 5},
            "info:FRQ_SWEGEN_DELLY": {"gt": 0.005}
        },
        "indb": {
            "info:OCC_INDB_DELLY": {"gt": 5},
            "info:FRQ_INDB_DELLY": {"gt": 0.005}
        }
        },
    "tiddit": {
        "swegen": {
            "info:OCC_SWEGEN_TIDDIT": {"gt": 5},
            "info:FRQ_SWEGEN_TIDDIT": {"gt": 0.005}
        },
        "indb": {
            "info:OCC_INDB_TIDDIT": {"gt": 5},
            "info:FRQ_INDB_TIDDIT": {"gt": 0.005}
        }
        },
}


RESCUE_FILTERS = {
    "exception_DEL_on_X": {
        "info:SVTYPE": {
            "in": ["DEL"]
        },
        "chrom": {
            "in": ["X"]
            },
        "format:GT": {
            "in": ["1", "1/1", "1|1"]
        }
    },
    "exception_ACMG_class": {
        "info:ACMG_class": {
            "in": ["4", "5"]
        }
    }
}


# Caller-specific VCF record lines to make tests for
# Exception filters are tested:
# - rescue DEL on hemizygote X: CNVnator_del_23
# - rescue ACMG_class>=4: DRAGEN:LOSS:1:14437062-14438764
BODY_PRE = {
    "cnvnator": [
        "1	1580126	CNVnator_dup_23	G	<DUP>	.	PASS	END=1583750;SVTYPE=DUP;SVLEN=3625;IMPRECISE;natorRD=1.63608;natorP1=3.64996e-06;natorP2=558.547;natorP3=0.322035;natorP4=0.00393917;natorQ0=0.0663442;CNVNATORID=CNVnator_dup_23;OCC_INDB_CNVNATOR=1;FRQ_INDB_CNVNATOR=0.025;OCC_GNOMAD=0;FRQ_GNOMAD=0.0	GT:CN	./.:2",
        "X	1580127	CNVnator_del_23	G	<DEL>	.	PASS	END=1583752;SVTYPE=DEL;SVLEN=3626;IMPRECISE;natorRD=0.03608;natorP1=3.64996e-06;natorP2=558.547;natorP3=0.322035;natorP4=0.00393917;natorQ0=0.0663442;CNVNATORID=CNVnator_del_23;OCC_INDB_CNVNATOR=1;FRQ_INDB_CNVNATOR=0.025;OCC_GNOMAD=5587;FRQ_GNOMAD=0.726485	GT:CN	1/1:0"
    ],
    "manta": [
        "1	1413234	MantaDUP:TANDEM:8698:0:1:0:1:0	C	<DUP:TANDEM>	496	PASS	END=1413364;SVTYPE=DUP;SVLEN=130;SVINSLEN=3;SVINSSEQ=TGT;MANTAID=MantaDUP:TANDEM:8698:0:1:0:1:0;CIPOS=-2,0;CIEND=0,1;COVM=53.3092460632;COVA=47.6257705688;COVB=45.7140922546;LFA=16;LFB=11;LTE=8;OR=0,0,0,0;ORSR=0,8;QUALA=55;QUALB=53;TIDDITID=SV_5_1;OCC_SWEGEN_MANTA=985;FRQ_SWEGEN_MANTA=0.852557673019;OCC_INDB_MANTA=1;FRQ_INDB_MANTA=0.025;OCC_INDB_TIDDIT=1;FRQ_INDB_TIDDIT=0.03	GT:FT:GQ:PL:PR:SR	0/1:PASS:496:546,0,621:14,0:46,24"
    ],
    "delly": [
        "1	756180	DEL00000001	A	<DEL>	127	PASS	IMPRECISE;SVTYPE=DEL;SVMETHOD=EMBL.DELLYv0.8.3;END=758862;PE=5;MAPQ=26;CT=3to5;CIPOS=-246,246;CIEND=-246,246;SVLEN=-2682;DELLYID=DEL00000001;OCC_SWEGEN_DELLY=955;FRQ_SWEGEN_DELLY=0.268703898841;OCC_INDB_DELLY=1;FRQ_INDB_DELLY=0.025	GT:GL:GQ:FT:RCL:RC:RCR:CN:DR:DV:RR:RV	0/1:-4.96917,0,-130.099:50:PASS:191:389:209:2:24:6:0:0"
    ],
    "tiddit": [
        "1	3216310	SV_13_1	N	<DEL>	4	PASS	SVTYPE=DEL;CIPOS=-104,0;CIEND=0,223;END=3226965;SVLEN=-10656;COVM=13.4474010468;COVA=27.3679275513;COVB=44.7242279053;LFA=8;LFB=8;LTE=8;OR=0,0,0,8;ORSR=0,0;QUALA=29;QUALB=53;TIDDITID=SV_13_1;OCC_SWEGEN_DELLY=259;FRQ_SWEGEN_DELLY=0.27291886196;OCC_SWEGEN_TIDDIT=100;FRQ_SWEGEN_TIDDIT=0.1;OCC_INDB_TIDDIT=20;FRQ_INDB_TIDDIT=0.5	GT:CN:DV:RV:DR:RR	0/1:1:8:0:17,39:14,27"
    ],
    "canvas": [
         "1	14437061	DRAGEN:LOSS:1:14437061-14438764	N	<DEL>	16	cnvLength	SVTYPE=DEL;END=14438764;REFLEN=1703;CANVASID=DRAGEN:LOSS:1:14437061-14438764;SVLEN=-1703;OCC_SWEGEN_CNVNATOR=259;FRQ_SWEGEN_CNVNATOR=0.27291886196;OCC_INDB_CANVAS=10;FRQ_INDB_CANVAS=0.25;OCC_GNOMAD=5587;FRQ_GNOMAD=0.726485	GT:SM:CN:BC:PE	0/1:0.367212:1:6:36,49",
         "1	14437061	DRAGEN:LOSS:1:14437061-14438764	N	<DEL>	16	PASS	SVTYPE=DEL;END=14438764;REFLEN=1703;CANVASID=DRAGEN:LOSS:1:14437061-14438764;SVLEN=-1703;OCC_SWEGEN_CNVNATOR=259;FRQ_SWEGEN_CNVNATOR=0.27291886196;OCC_INDB_CANVAS=10;FRQ_INDB_CANVAS=0.25;OCC_GNOMAD=5587;FRQ_GNOMAD=0.726485	GT:SM:CN:BC:PE	0/1:0.367212:1:6:36,49",
         "1	14437062	DRAGEN:LOSS:1:14437062-14438764	N	<DEL>	16	PASS	SVTYPE=DEL;END=14438764;REFLEN=1702;CANVASID=DRAGEN:LOSS:1:14437062-14438764;SVLEN=-1702;OCC_INDB_CANVAS=12;FRQ_INDB_CANVAS=0.25;ACMG_class=4	GT:SM:CN:BC:PE	0/1:0.367212:1:6:36,49"
    ]
}

# Expected value in VCF FILTER column after frequency annotation
FILTER_POST = {
    "cnvnator": [
        "PASS", "HiFreqGnomad"
    ],
    "manta": [
        "HiFreqSwegen",
    ],
    "delly": [
        "HiFreqSwegen"
    ],
    "tiddit": [
        "HiFreqInHdb;HiFreqSwegen"
    ],
    "canvas": [
        "cnvLength;HiFreqInHdb;HiFreqSwegen;HiFreqGnomad",
        "HiFreqInHdb;HiFreqSwegen;HiFreqGnomad",
        "HiFreqInHdb"
    ]
}

# Expected extraction of variant evidence from VCF records
EVIDENCE_POST = {
    "cnvnator": [
        (None, None, "1.63608", None, "2", "./."),
        (None, None, "0.03608", None, "0", "1/1")
    ],
    "manta": [
        ("46,24", "14,0", None, "0", None, "0/1"),
    ],
    "delly": [
        ("0,0", "24,6", "0.9701", "6", "2", "0/1")
    ],
    "tiddit": [
        ("(14,27),0", "(17,39),8", "0.1840", "8", "1", "0/1")
    ],
    "canvas": [
        (None, "36,49", "0.367212", None, "1", "0/1"),
        (None, "36,49", "0.367212", None, "1", "0/1"),
        (None, "36,49", "0.367212", None, "1", "0/1")
    ]
}


# Include based on frequency in INFO-tag in BODY_PRE
# I.e. Keep when True, remove when False
# No-filter for CNVnator homozygote DEL on X is an exception
FREQUENCY_POST = {
    "cnvnator": [
        True, True
    ],
    "manta": [
        False
    ],
    "delly": [
        False
    ],
    "tiddit": [
        False
    ],
    "canvas": [
        False, False, True
    ]
}


# Here: Remove variants not in INTERPRETATION_GROUPS:
# [small, medium, large, default]
# Keep when True, remove when False
INTERPRETATION_POST = {
    "cnvnator": [
        [False, True, False, True],
        [False, True, False, True]
    ],
    "manta": [
        [True, False, False, True],
    ],
    "delly": [
        [False, True, False, True]
    ],
    "tiddit": [
        [False, False, True, True]
    ],
    "canvas": [
        [False, False, False, True],
        [True, False, False, True],
        [True, False, False, True]
    ]
}

# Expected TSV formatted output for VCF records
BODY_POST_TSV = {
    "cnvnator": [
        "1	1580126	1583750	CNVnator_dup_23	DUP	3625	PASS	1:1580126-1583750	0	0	0	1	0	0	0	0	0	0.0	.	IMPRECISE	None	None	None	1.63608	None	0.0663442	None	None	None	CNVnator_dup_23	None	2	./.	None",
        "X	1580127	1583752	CNVnator_del_23	DEL	3626	PASS	X:1580127-1583752	0	0	0	1	0	0	0	0	0	0.726485	.	IMPRECISE	None	None	None	0.03608	None	0.0663442	None	None	None	CNVnator_del_23	None	0	1/1	None"
    ],
    "manta": [
        "1	1413234	1413364	MantaDUP:TANDEM:8698:0:1:0:1:0	DUP	130	PASS	1:1413234-1413364	1	0	1	0	0	0.852557673019	0	0	0	0	496	PRECISE	None	46,24	14,0	None	0	None	MantaDUP:TANDEM:8698:0:1:0:1:0	None	SV_5_1	None	None	None	0/1	None"
    ],
    "delly": [
        "1	756180	758862	DEL00000001	DEL	2682	PASS	1:756180-758862	0	1	0	0	0	0	0.268703898841	0	0	0	127	IMPRECISE	None	0,0	24,6	0.9701	6	None	None	DEL00000001	None	None	None	2	0/1	None"
    ],
    "tiddit": [
        "1	3216310	3226965	SV_13_1	DEL	10656	PASS	1:3216310-3226965	0	0	20	0	0	0	0.27291886196	0.1	0	0	4	IMPRECISE	None	(14,27),0	(17,39),8	0.1840	8	None	None	None	SV_13_1	None	None	1	0/1	None"
    ],
    "canvas": [
        "1	14437061	14438764	DRAGEN:LOSS:1:14437061-14438764	DEL	1703	cnvLength	1:14437061-14438764	0	0	0	0	10	0	0	0	0.27291886196	0.726485	16	IMPRECISE	None	None	36,49	0.367212	None	None	None	None	None	None	DRAGEN:LOSS:1:14437061-14438764	1	0/1	None",
        "1	14437061	14438764	DRAGEN:LOSS:1:14437061-14438764	DEL	1703	PASS	1:14437061-14438764	0	0	0	0	10	0	0	0	0.27291886196	0.726485	16	IMPRECISE	None	None	36,49	0.367212	None	None	None	None	None	None	DRAGEN:LOSS:1:14437061-14438764	1	0/1	None",
        "1	14437062	14438764	DRAGEN:LOSS:1:14437062-14438764	DEL	1702	PASS	1:14437062-14438764	0	0	0	0	12	0	0	0	0	0	16	IMPRECISE	None	None	36,49	0.367212	None	None	None	None	None	None	DRAGEN:LOSS:1:14437062-14438764	1	0/1	4"
    ]
}

CALLER_PRIORITY = ["manta", "canvas", "tiddit", "delly", "cnvnator"]

# Make pairs of input and output
def make_body_filter_pairs(post_data):
    return zip(it.chain.from_iterable(BODY_PRE.values()),
               it.chain.from_iterable(post_data.values()))


# Check that frequency tags are added to FILTER
@pytest.mark.parametrize("pre_line, post_filter",
                         make_body_filter_pairs(FILTER_POST))
def test_add_frquency_filter(pre_line, post_filter):
    (record, _) = parse_record(pre_line)
    assert add_frequency_filter(record,
                                FREQUENCY_FILTERS,
                                caller_priority=CALLER_PRIORITY
                                ) == post_filter, pre_line


# Check that filters are correctly added
@pytest.mark.parametrize("pre_line, post_filter",
                         make_body_filter_pairs(FILTER_POST))
def test_do_sv_wgs_filtering_add_frequency_filter(pre_line, post_filter):
    kwargs = dict(output_format="vcf",
                  frequency_filters=FREQUENCY_FILTERS,
                  caller_priority=CALLER_PRIORITY,
                  debug=True)
    formatted_line = "\n".join(do_sv_wgs_filtering(io.StringIO(str(pre_line)), **kwargs))

    assert pre_line.split("\t")[7:] == formatted_line.split("\t")[7:], pre_line
    assert pre_line.split("\t")[:6] == formatted_line.split("\t")[:6], pre_line
    assert formatted_line.split("\t")[6] == post_filter, pre_line


# Check that variant evidence is correctly parsed
@pytest.mark.parametrize("pre_line, post_evidence",
                         make_body_filter_pairs(EVIDENCE_POST))
def test_format_evidence(pre_line, post_evidence):
    (record, samples) = parse_record(pre_line)
    evidence = format_evidence(record, list(samples))
    assert evidence == post_evidence, parse_record(pre_line)


# Check TSV formatting
@pytest.mark.parametrize("pre_line, post_line",
                         make_body_filter_pairs(BODY_POST_TSV))
def test_tsv_format_output(pre_line, post_line):
    formatted_line = format_record_tsv(*parse_record(pre_line))
    assert formatted_line == post_line, parse_record(pre_line)


# Check that TSV formatting is the same when run from command line
@pytest.mark.parametrize("pre_line, post_line",
                         make_body_filter_pairs(BODY_POST_TSV))
def test_main_tsv_format_output(pre_line, post_line):
    kwargs = dict(output_format="tsv")
    formatted_line = "\n".join(do_sv_wgs_filtering(io.StringIO(str(pre_line)), **kwargs))
    assert formatted_line == post_line, parse_record(pre_line)


# Check that variant records are placed in the correct interpretation group
@pytest.mark.parametrize("pre_line, post_interpretation",
                         make_body_filter_pairs(INTERPRETATION_POST))
def test_interpretation_filters(pre_line, post_interpretation):
    (record, samples) = parse_record(pre_line)

    post = list(
        map(lambda g: select_for_interpretation(record, samples, group=g), INTERPRETATION_GROUPS.values())
    )
    assert post == post_interpretation, pre_line


# Check that interpretation groups can also be provided on commnd line
@pytest.mark.parametrize("pre_line, post_interpretation",
                         make_body_filter_pairs(INTERPRETATION_POST))
def test_main_interpretation_filters(pre_line, post_interpretation):
    post = list(
        map(
            lambda ig: bool("".join(do_sv_wgs_filtering(io.StringIO(str(pre_line)),
                                                interpretation_group=ig))),
            INTERPRETATION_GROUPS.values()
            )
    )
    assert post == post_interpretation, pre_line


# Check that frequency filtering works
@pytest.mark.parametrize("pre_line, post_frequency",
                         make_body_filter_pairs(FREQUENCY_POST))
def test_frequency_filters(pre_line, post_frequency):
    (record, samples) = parse_record(pre_line)
    post = frequency_filter(record,
                            samples,
                            filters=FREQUENCY_FILTERS,
                            exceptions=RESCUE_FILTERS,
                            caller_priority=CALLER_PRIORITY
                            )
    assert post == post_frequency, parse_record(pre_line)


# Check that frequency filtersing works also when providing
# frequency filters from command line
@pytest.mark.parametrize("pre_line, post_frequency",
                         make_body_filter_pairs(FREQUENCY_POST))
def test_main_frequency_filters(pre_line, post_frequency):
    kwargs = dict(output_format="vcf",
                  frequency_filters=FREQUENCY_FILTERS,
                  rescue_filters=RESCUE_FILTERS,
                  caller_priority=CALLER_PRIORITY,
                  debug=False)
    post = do_sv_wgs_filtering(io.StringIO(str(pre_line)), **kwargs)
    assert any(post) == post_frequency, parse_record(pre_line)
