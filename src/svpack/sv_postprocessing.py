#!/usr/bin/env python

"""sv_postprocessing.py: Script that updates the FILTER column
                         according to quality filters provided in
                         `analysistypeconfig.json` in `anno-targets`
"""

import sys
import argparse
import json
import ast
import logging
import itertools as it

from collections import OrderedDict

from svpack import sv_utils as utils

_log = logging.getLogger(__name__)


def input_parser():
    parser = argparse.ArgumentParser(description='Postprocessing of annotated VCF')
    parser.add_argument('file', nargs='?', type=argparse.FileType('r'),
                        default=sys.stdin, help='VCF filename')
    parser.add_argument('--set-filters', type=str, default="{}",
                        help='JSON-formatted string to override: %(default)s')
    parser.add_argument('--set-filter-descriptions', type=str, default='{}',
                        help='JSON-formatted string to override: %(default)s')
    parser.add_argument('--caller-priority', type=str, nargs="+",
                        default=["manta", "canvas", "tiddit", "delly", "cnvnator"],
                        help='JSON-formatted string to override: %(default)s')
    parser.add_argument('--debug', action='store_true',
                        help="Get output on debug form")
    return parser.parse_args()


############################################################
# Select subset of variants based on INTERPRETATION GROUPS #
############################################################
def _get_csq_field(csq,
                   field_number):
    """
    Get iterator of annotations in CSQ 'field_number'
    """
    vep_annotations = csq.split(",")
    return list(map(lambda s: s.split("|").pop(field_number), vep_annotations))


##############################################
# Get annotations and set filtering based on it #
##############################################
def _has_called(caller, record):
    """
    Return True if caller has called
    Identified by presence of CALLERID (set by sv_standardizer)
    """
    caller_id = f"{caller.upper()}ID"
    return caller_id in record["info"]


def get_priority_caller(record, caller_priority):
    """
    Get prioritized caller
    """
    has_called = list(
        filter(lambda c: _has_called(c, record), caller_priority)
        )
    if has_called:
        return has_called[0]
    else:
        return None


# Parse FILTER values 
def get_quality_filter(record,
                       samples,
                       filter_name,
                       filters={},
                       caller_priority=[]):
    """
    Return FILTER value for low quality filter
    Prioritize CALLER specific filter over "common" if both exists

    filters = {
        filter_name: {
            caller: {
                filter_alt1: {filter_type: {relation: value}}
                },
                {
                filter_alt2: {filter_type: {relation: value}}
                }
            },
            "common": {
                filter_alt1: {filter_type: {relation: value}}
                }
            },
            exceptions: {
                exception_name1: {filter_type: {relation: value}
                },
                {
                exception_name2: {filter_type: {relation: value}
                }
    }
    }
    """
    filter_definition = filters.get(filter_name, {})
    
    common_filters = filter_definition.get("common", {})

    priority_caller = get_priority_caller(record, caller_priority)

    priority_filters = filter_definition.get(priority_caller, common_filters)

    exceptions = filter_definition.get("exceptions", {})

    if utils.filter_body(record, samples, exceptions, group_names=[]):
        return "PASS"
    elif utils.filter_body(record, samples, priority_filters, group_names=[]):
        return filter_name
    else:
        return "PASS"


#########################
# Add new FILTER to VCF #
#########################
def make_filter_header(filter_name,
                       descriptions):
    """
    Make a VCF FILTER header for filter_name
    Provide specific descriptions if needed
    """
    filter_description = descriptions.get(filter_name, f"Custom filter {filter_name}")
    header_def = dict(ID=filter_name,
                      Description=f'"{filter_description}"')
    return (f"##FILTER=<ID={filter_name}", header_def)


def add_headers(filter_names,
                descriptions={}):
    """
    Add header lines when introducing new fields in VCF
    """

    return list(
        map(
            lambda filter_name: make_filter_header(filter_name, descriptions),
            filter_names
            )
    )


def complement_header(header,
                      added_headers):
    """
    Parse header.
    Optionally add new header definitions to output
    """
    parsed_header = map(utils.parse_header, header)
    return utils.complement_header(parsed_header,
                                   added_headers)


# Mark hi freq variants for manual inspection
def add_quality_filter(record,
                       samples,
                       filters,
                       caller_priority=[]):
    """
    Add filter to FILTER column if low quality
    """

    added_filters = map(
        lambda filter_name: get_quality_filter(record,
                                               samples,
                                               filter_name,
                                               filters=filters,
                                               caller_priority=caller_priority),
        filters.keys()
        )

    new_filters = OrderedDict.fromkeys(
        record["filter"].split(";") + list(added_filters)
        )

    if all([len(new_filters) > 1,
            "PASS" in new_filters]):
        new_filters.pop("PASS")

    FILTER = ";".join(new_filters)

    return FILTER


def mark_record_for_filtering(record,
                              samples,
                              filters,
                              caller_priority):
    """
    * Update FILTER column to indicate low quality
    """
    record["filter"] = add_quality_filter(record,
                                          samples,
                                          filters=filters,
                                          caller_priority=caller_priority)
    return (record, samples)


def filter_header_body(is_header,
                       records,
                       filters,
                       caller_priority,
                       filter_descriptions):
    """
    Filter either header lines or body lines
    """
    if is_header:
        return complement_header(records,
                                 add_headers(filters.keys(),
                                             filter_descriptions))
    else:
        # Make iterator 'parsed_body'
        parsed_body = map(utils.parse_record, records)

        
        # Mark variants if they are low quality (by updating 'parsed_body')
        if filters:
            # Overwrite 'parsed_body'
            parsed_body = map(
                lambda rs: mark_record_for_filtering(*rs,
                                                     filters,
                                                     caller_priority=caller_priority), parsed_body
            )

        return it.starmap(utils.format_record, parsed_body)


def do_sv_postprocessing(file_object,
                        filters={},
                        caller_priority=[],
                        filter_descriptions={}):
    """
    * Filter input
    * Format output
    """
    # Split vcf file_object iterator into
    # [('True', header), ('False', body)]
    f_split = it.groupby(file_object, key=lambda line: line.startswith("#"))

    # Processing ('True', header) and ('False', body)
    output_it = map(lambda f_group:
                    filter_header_body(*f_group,
                                       filters,
                                       caller_priority,
                                       filter_descriptions),
                    f_split)

    # Flatten [ header_post, filtered_body ] into iterator
    return it.chain.from_iterable(output_it)


def main():
    args = input_parser()
    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    _log.info(f"Run parameters:\n{json.dumps(vars(args), indent=2, cls=utils.JSONArgparseEncoder)}")

    # Using ast.literal_eval for JSON-style strings, because Bash-variables
    # are often written on form  "{'key': item}" and not '{"key": item}'
    filters = ast.literal_eval(args.set_filters)
    filter_descriptions = ast.literal_eval(args.set_filter_descriptions)

    with args.file:
        output = do_sv_postprocessing(args.file,
                                     filters=filters,
                                     caller_priority=args.caller_priority,
                                     filter_descriptions=filter_descriptions)

        print(*output, sep="\n")


if __name__ == "__main__":
    main()
