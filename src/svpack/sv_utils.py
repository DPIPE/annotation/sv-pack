#!/usr/bin/env python

"""sv_utils.py: Functions for parsing of VCFs and adding new headers
"""

import io
import logging
import re
import itertools as it
from collections import OrderedDict
from json import JSONEncoder

_log = logging.getLogger(__name__)


##############################################
# Utility functions used by multiple scripts #
##############################################
class JSONArgparseEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, io.IOBase):
            # Format input stream
            if hasattr(o, "name"):
                # As filename
                return o.name
            else:
                # As <stdin>
                return repr(o)
        else:
            return o


#####################################################################
# Utility function that is used by for both header and body parsing #
#####################################################################
def _info_splitter(field,
                   sep="="):
    """
    Split info field on form: ID='value'
    Special care when no 'value'
    """
    split_field = field.split(sep, 1)
    assert len(split_field) in [1, 2]

    if len(split_field) == 2:
        return tuple(split_field)
    else:
        return (split_field[0], True)


##############################################
# Utility functions for parsing header lines #
##############################################
def parse_header(line):
    """
    Parse header lines of VCF
    """
    line = line.strip()
    if line.startswith(("##INFO", "##FORMAT")):
        _key, value = line.split("=", 1)
        _type, _ = line.split(",", 1)
        return (_type, dict(map(_info_splitter, value.strip("<>").split(",", 3))))
    elif line.startswith(("##contig", "##FILTER", "##ALT")):
        _key, value = line.split("=", 1)
        _type, _ = line.split(",", 1)
        return (_type, dict(map(_info_splitter, value.strip("<>").split(",", 1))))
    elif line.startswith("#CHROM"):
        _type = line.split("\t", 1)[0]
        return (_type, dict(SAMPLES=line.split()[9:]))
    else:
        # _key, value = line.split("=", 1)
        _type = line.strip()
        return (_type, dict(CUSTOM=line))


def _get_header_type(header_tuple):
    """
    Parse header record to get type (##INFO, ##FORMAT, etc)
    """
    header_type, _ = header_tuple
    return header_type.split("=", 1)[0]


def _group_header_by_type(header):
    """
    Group headers by type (##INFO, ##FORMAT, etc)
    """
    return it.starmap(
        lambda g, h: (g, list(h)),
        it.groupby(
            header, key=_get_header_type
        )
    )


def format_header(_type,
                  value):
    """
    Format header records as VCF lines
    """
    filter_header = '##FILTER=<ID={ID},Description={Description}>'
    info_header = '##INFO=<ID={ID},Number={Number},Type={Type},Description={Description}>'
    alt_header = '##ALT=<ID={ID},Description={Description}>'
    format_header = '##FORMAT=<ID={ID},Number={Number},Type={Type},Description={Description}>'
    contig_header = '##contig=<ID={ID},length={length}>'
    columns = ['#CHROM', 'POS', 'ID', 'REF',
               'ALT', 'QUAL', 'FILTER', 'INFO',
               'FORMAT']

    if _type.startswith("##FORMAT"):
        return format_header.format(**value)
    elif _type.startswith("##INFO"):
        return info_header.format(**value)
    elif _type.startswith("##ALT"):
        return alt_header.format(**value)
    elif _type.startswith("##FILTER"):
        return filter_header.format(**value)
    elif _type.startswith("##contig"):
        return contig_header.format(**value)
    elif _type.startswith("#CHR"):
        return "\t".join(columns + value["SAMPLES"])
    else:
        return value["CUSTOM"]


###################################################
# Utility function for adding new lines to header #
###################################################
def complement_header(standardized_header,
                      added_header):
    """
    Complete header defintions
    Return iterator of header lines
    """
    _headers = _group_header_by_type(
            it.chain(standardized_header, added_header)
        )

    # Update header on a per type basis (types are ##INFO, ##FORMAT, etc)
    _header_per_type = OrderedDict()
    for _type, values in _headers:
        _header_per_type.setdefault(_type, []).extend(values)

    # Ensure uniqueness of header definitions
    for _type in _header_per_type:
        _header_per_type[_type] = OrderedDict(_header_per_type[_type]).items()

    # Get #CHROM column header
    _chrom_line = it.starmap(
        format_header,
        _header_per_type.pop("#CHROM")
    )

    # Get header lines
    _header_lines = it.starmap(
        format_header,
        it.chain.from_iterable(_header_per_type.values())
    )
    return it.chain(
        _header_lines, _chrom_line
    )


###############################################################
# Utility function for parsing and formatting variant records #
###############################################################
def parse_record(line):
    """
    Parse VCF line into
    * variant centered part
    * sample centered part
    """
    CHROM, POS, ID, REF, ALT, QUAL, FILTER, INFO, FORMAT = line.split("\t")[:9]

    SAMPLES = line.strip().split("\t")[9:]

    record = {"chrom": CHROM,
              "pos": POS,
              "id": ID,
              "ref": REF,
              "alt": ALT,
              "qual": QUAL,
              "filter": FILTER,
              "info":  OrderedDict(map(_info_splitter, INFO.split(";"))),
              "format": FORMAT}

    if SAMPLES:
        samples = list(
            map(OrderedDict,
                it.starmap(lambda f, s: zip(f.split(":"), s.split(":")),
                           it.product([FORMAT], SAMPLES)
                           )
                )
            )
    else:
        samples = []
    return (record, samples)


def _info_joiner(_id,
                 value):
    """
    Assemble an INFO field
    """
    if not isinstance(value, bool):
        return "{_id}={value}".format(_id=_id,
                                      value=value)
    else:
        return _id


def format_record(record,
                  samples):
    """
    Format a record dict as a VCF record line
    """
    CHROM = record["chrom"]
    POS = record["pos"]
    ID = record["id"]
    REF = record["ref"]
    ALT = record["alt"]
    QUAL = record["qual"]
    FILTER = record["filter"]
    INFO = ";".join(it.starmap(_info_joiner, record["info"].items()))
    FORMAT = record["format"]

    SAMPLES = list(
        map(lambda s: ":".join(s),
            map(OrderedDict.values, samples))
    )

    return "\t".join([CHROM, POS, ID, REF, ALT, QUAL, FILTER, INFO, FORMAT] + SAMPLES)


###############################################
# Utility function for user defined filtering
###############################################
def filter_body(record, samples, filters, group_names=[]):
    """
    Try selection of filter groups to check if any of them match
    filters = {
        group_name: {
            filter_name: {filter_type: {relation: value}}
            }
        group_name: {
            filter_name: {filter_type: {relation: value}}
            }
    }
    """
    if group_names:
        filter_selection = map(filters.get, group_names)
    else:
        filter_selection = filters.values()

    return any(
        map(lambda ff: check_filter_relations(record, samples, ff), filter_selection)
        )


def check_filter_relations(record, samples, filter_group):
    """
    Check all filter relations from one filter group
    filter_group = {
        filter_name_0: {
            filter_type: {relation: value}
        },
        filter_name_i: {
            filter_type: {relation: value}
        }
    }
    """
    return all(
        map(lambda _fi: check_filter_relation(record, samples, *_fi), filter_group.items())
        )


def check_filter_relation(record,
                          samples,
                          filter_type,
                          filter_relations):
    """
    Check all filter relations for one filter type:
    filter_relations = {
        relation_0: value,
        relation_i: value
        }
    """
    if filter_type.startswith(("info", "format")):
        vcf_column, _vcf_key_id = filter_type.split(":")
    else:
        vcf_column = filter_type

    assert vcf_column in record, f"{vcf_column} not in {record}"

    if vcf_column == "format":
        return check_sample_relations(samples,
                                      _vcf_key_id,
                                      filter_relations)
    else:
        if vcf_column == "info":
            value = record[vcf_column].get(_vcf_key_id, None)
        else:
            value = record[vcf_column]

        return check_relations(value,
                               filter_relations)


def check_relations(value,
                    filter_relations):
    """
    Check a particular value against a dict of filter_relations
    Do not filter if value is None
    """
    if value is None:
        return False
    else:
        return all(
            map(
                lambda _fr: check_relation(*parse_relation(value, *_fr)),
                filter_relations.items()
                )
        )


def check_sample_relations(samples,
                           vcf_column,
                           filter_relations):
    """
    Run check_relations() for all samples
    """
    return any(
        map(lambda sample: check_relations(sample.get(vcf_column, None),
                                           filter_relations), samples)
        )


def parse_relation(value, relation, filter_value):
    """
    Allow only the following:
    * Numerical filters with positive floats only
    * Force value as positive float and filter_value as float
    * floats accept all _relations in dir(float)
    * Special purpose filter relations:
    * When relation is 'in':
        * For checking entire annotation strings (inverts value and filter_value)
    * When relation is 'contains':
        * For checking FILTER (';' interpreted as separator, match by one or more items)
    * When relation is 'regex':
        * For checking INFO=CSQ (',' interpreted as separator, match once per part)
    """
    def set_type(value):
        """
        * Set numerical values to floats
        * Otherwise raise error
        TODO: typing based on VCF header
        """
        try:
            return float(value)
        except ValueError:
            return ValueError(f"float({value}) is not defined")
        except TypeError:
            raise TypeError(f"float({value}) is not defined")

    err_msg1 = f"Relation '{relation}' requires list of strings, got: {filter_value}"
    err_msg1b = f"Relation '{relation}' requires string, got: {filter_value}"
    err_msg2 = f"Negative {filter_value} is not defined"

    if relation == "in":
        assert isinstance(filter_value, list), err_msg1
        assert all(map(lambda fv: isinstance(fv, str), filter_value)), err_msg1

        # Invert order of value and filter_value
        relation_name = "__contains__"
        return filter_value, relation_name, value
    elif relation == "contains":
        assert isinstance(filter_value, list), err_msg1
        assert all(map(lambda fv: isinstance(fv, str), filter_value)), err_msg1

        relation_name = "__and__"
        return set(value.split(";")), relation_name, set(filter_value)
    if relation == "search":
        assert isinstance(filter_value, str), err_msg1b

        relation_name = "__eq__"
        return all(map(re.compile(f"{filter_value}").search, value.split(","))), relation_name, True
    else:
        relation_name = f"__{relation}__"

        tvalue = abs(set_type(value))

        filter_tvalue = set_type(filter_value)
        assert filter_tvalue >= 0.0, err_msg2

        return tvalue, relation_name, filter_tvalue


def check_relation(value, relation, filter_value):
    """
    Compare value and filter_value with respect to relation
    """
    return bool(getattr(value, relation)(filter_value))
